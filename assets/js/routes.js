var appRoutes = [
    {
        path: "/",
        name: "index",
        component: "index-view"
    },
    {
        path: "/task-view/:id",
        name: "task",
        component: "task-view"
    },
    {
        path: "/create-task",
        name: "create-task",
        component: "create-task"
    },
    {
        path: "/activity",
        name: "activity",
        component: "activity"
    },
    {
        path: "/trash",
        name: "trash",
        component: "trash"
    },
    {
        path: "/notes",
        name: "notes",
        component: "notes"
    },
    {
        path: "/labels",
        name: "labels",
        component: "labels"
    },
    {
        path: "/done",
        name: "done",
        component: "done"
    },
    {
        path: "/not-found-404",
        name: "not-found-404",
        component: "not-found-404"
    },
    {
        path: "/users",
        name: "users",
        component: "users"
    },
    {
        path: "/restricted",
        name: "restricted",
        component: "restricted"
    }
]