(function () {
    var LocStorStats = function (collectionName, type) {
        this.active = true;
        this.type = type || 'sessionStorage';
        this.collectionName = collectionName;
        this.stats = {}
        this.maxCapacity = geConfig.gannerc[this.type + 'CacheMaxCapacity'] || 5242880; // 5Mb default

        this.init = function (collectionName) {
            if (undefined === window[this.type]) this.active = false;
            if (!this.getStats()) this.genStats();
        }

        this.refreshStats = function () {
            this.stats = {}
            this.genStats();
        }

        this.getStats = function () {
            var stats = window[this.type].getItem(this.collectionName + "_stats");
            var statsObj;
            if (stats) {
                try {
                    statsObj = JSON.parse(stats);
                } catch (error) {
                }
                if (undefined !== statsObj) {
                    this.stats = statsObj;
                }
                return statsObj;
            }

            return false;
        }

        this.clearCache = function (forceRefresh) {
            for (var i in this.stats.items) {
                var keyName = this.stats.items[i];
                var item = window[this.type].getItem(keyName);
                var now = parseInt(Date.now() / 1000);
                var refresh = false;
                if (item) {
                    try {
                        itemObj = JSON.parse(item);
                    } catch (error) {
                    }
                    if (undefined !== itemObj) {
                        var outdated = itemObj.alive - now < 0;
                        if (outdated) {
                            window[this.type].removeItem(keyName);
                            var app = window[window.geConfig.gannerc.appVarName];
                            if (app) {
                                app.bus.$emit('cache-cleared', keyName);
                            }

                            refresh = true;
                        }
                    }
                }
            }

            if (refresh || forceRefresh) {
                this.refreshStats();
            }
        }

        this.addCheck = function (stringItem) {
            var capacityAvailable = this.stats.capacityLeft - stringItem.length > 0;
            return capacityAvailable;
        }

        this.calculateStorageSpace = function () {
            this.stats.capacityLeft = this.maxCapacity - (this.stats.bytes + this.stats.bytes2)
        }

        this.genStats = function () {
            var keys = Object.keys(window[this.type]);
            var regex = new RegExp(this.collectionName + '_', 'gm');
            var items = [];
            var itemsSizes = {};
            var bytes = 0;
            var bytes2 = 0; //other elements ( outside of )

            for (var i = 0; i < keys.length; i++) {
                var keyName = keys[i];
                var item = window[this.type].getItem(keyName);
                var matched = typeof regex.exec(keyName) === "object";
                if (matched) {
                    items.push(keyName);
                    bytes += item.length;
                    itemsSizes[keyName] = item.length;
                } else {
                    bytes2 += item.length;
                }
                delete matched;
            }
            this.stats = {
                itemsSizes: itemsSizes,
                items: items,
                bytes: bytes,
                bytes2: bytes2
            }
            this.calculateStorageSpace();
            this.saveStats();
        }

        this.saveStats = function () {
            window[this.type].setItem(this.collectionName + '_stats', JSON.stringify(this.stats));
        }


        return this.init(collectionName);
    };

    window.LocStorStats = LocStorStats;
})()