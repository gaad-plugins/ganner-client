(function (Vue) {

    /*
    * fix for no cached version
    * */
    Vue.config.warnHandler = (err, vm, info) => {
        var regex = /Unknown custom element: <.*>/gm;
        var app = window[window.geConfig.gannerc.appVarName];

        if (undefined !== app && regex.exec(err) !== null) {
            app.loading = true;
            setTimeout(function () {
                app.loading = false;
            }, 10);
            return;
        }

        //default behavior
        console.error(("[Vue warn]: " + err + info));
    };


    Vue.use(window.vuelidate.default);
//    Vue.component('tinymce', tinymce);


    String.prototype.camelize = function () {
        return this.replace(/(?:^\w|[A-Z]|\b\w)/g, function (word, index) {
            return index == 0 ? word.toLowerCase() : word.toUpperCase();
        }).replace(/\s+/g, '').split('-').join('');
    };

    String.prototype.hashCode = function () {
        var hash = 0;
        if (this.length == 0) {
            return hash;
        }
        for (var i = 0; i < this.length; i++) {
            var char = this.charCodeAt(i);
            hash = ((hash << 5) - hash) + char;
            hash = hash & hash; // Convert to 32bit integer
        }
        return Math.abs(hash);
    }

    /*
    * This executes queue of data loading that is requested from application components
     */
    function loadApiDataExecutor(app) {
        if (undefined !== app.app && undefined !== app.app.dataLoadingQueueEmpty) app = app.app;

        if (!app.dataLoadingQueueEmpty()) app.appDataLoading(true);
        var queue = app.dataLoadingQueue;
        for (i in queue) {
            var request = new apiDataRequest(i, queue[i], app);
            request.send();
        }
    }

    var bus = new Vue();

    window[window.geConfig.gannerc.appVarName] = new Vue({
        el: '#gannerc',
        store: Store,
        router: Router,
        data: {
            requestQueueExecutorInterval: false,
            lastRequestHash: {},
            pausePreload: false,
            manifestLoading: false,
            pushedStates: [],
            dataLoadingQueue: {},
            dataLoadedIndex: [],
            loading: false,
            loadingMsg: "",
            dataLoading: false,
            groutes: appRoutes,
            bus: bus,
            srcparam: false
        },

        created: function () {
            var cacheDisabled = geConfig.gannerc.sessionStorageCacheDisabled;
            if (!cacheDisabled) {
                var LocStorStats_ = new LocStorStats(geConfig.gannerc.localStorageCollectionName || 'gc', 'sessionStorage');
                LocStorStats_.clearCache(true);
            }

            if (false === this.requestQueueExecutorInterval) {
                this.requestQueueExecutorInterval = setInterval(loadApiDataExecutor, 100, {app: this});
            }

            this.dataLoading = true;
            this.srcparam = $(this.$options.el).attr('srcparam');

            this.bus.$on("routeChangeStart", this.appLoadingOn);
            this.bus.$on("routeChangeSuccess", this.appLoadingOff);
            this.bus.$on("dataLoadingDone", this.dataLoadingDone);
        },

        computed: {
            currentRoute: function () {
                //@TODO Implement 404 detection here
                return this.$route.query[this.srcparam];
            },
        },

        methods: {
            checkUserCaps: function (caps) {
                caps = typeof caps === "string" ? [caps] : caps;

                function containsOnly(array1, array2) {
                    return !array2.some(elem => !array1.includes(elem))
                }

                var allow = []
                var isAdmin = -1 !== window['geUserCaps']['roles'].indexOf('administrator');
                if (isAdmin) {
                    return true
                }

                for (var i in caps) {
                    var cap = caps[i];
                    var hasCap = -1 !== window['geUserCaps']['caps'].indexOf(cap);
                    allow.push(hasCap);
                }
                return containsOnly([true], allow);

            },

            registerStoreModule: function (originName, moduleData) {
                var moduleName = originName + "Module";
                var existingModule = Store.state[moduleName];
                if (existingModule) {
                    Store.state[moduleName] = moduleData;
                } else {
                    Store.registerModule(moduleName, {
                        state: moduleData
                    });
                }
            },

            getModulePreloadInterval: function (moduleName) {
                return undefined !== geConfig.gannerc.cachePreload[moduleName] ? geConfig.gannerc.cachePreload[moduleName] : geConfig.gannerc.cachePreload.default;
            },

            getModuleCacheAlive: function (moduleName) {
                return undefined !== geConfig.gannerc.cacheAlive[moduleName] ? geConfig.gannerc.cacheAlive[moduleName] : geConfig.gannerc.cacheAlive.default;
            },

            getModuleEndpoint: function (moduleName) {
                return undefined !== geConfig.gannerc.moduleDataEndpoint[moduleName] ? geConfig.gannerc.moduleDataEndpoint[moduleName] : null;
            },

            cacheClearedCallback: function (payload, keyName) {
                if (payload === keyName)
                    this.removeDataLoadedIndex(payload.replace(geConfig.gannerc.localStorageCollectionName + '_', '', payload));
            },

            removeDataLoadedIndex: function (name) {
                if (undefined === name || this.dataLoadedIndex.indexOf(name) === -1) return
                this.$store.unregisterModule(name + "Module");
                var aTmp = [];
                for (var i in this.dataLoadedIndex)
                    if (this.dataLoadedIndex[i] !== name) aTmp.push(this.dataLoadedIndex[i]);

                this.dataLoadedIndex = aTmp;
            },

            routes_: function () {
                var routes = [];
                for (var i = 0; i < this.groutes.length; i++) {
                    var indexes = [];
                    var route = this.groutes[i];
                    var parts = route.path.split("/");

                    for (var j in parts) {
                        var part = parts[j];
                        if (part === "") continue;
                        if (part[0] === ":") {
                            parts[j] = "(.*)";
                            indexes[j] = part.substr(1, part.length);
                        }
                    }
                    route["regexp"] = new RegExp("^" + parts.join("\\/") + "$", "i");
                    route["indexes"] = indexes;
                    route["paramsCount"] = indexes.filter(function (el) {
                        return el != null;
                    }).length;
                    routes[route.path] = route;
                }
                return routes;
            },

            getRoute: function (sRoute) {
                var sRoute = undefined === sRoute ? this.currentRoute : sRoute;
                var routes = this.routes_();
                var matchedRoutes = [];
                for (var path in routes) {
                    var route = routes[path];
                    var test = route.regexp.test(sRoute);
                    if (test) {
                        var matched = route.regexp.exec(sRoute);
                        var values = matched.splice(1, matched.length);
                        var params = {};
                        var parts = path.split("/");
                        var next = false;

                        for (partIndex in parts) {
                            var indexMatched = route.indexes[partIndex];
                            if (indexMatched) {
                                var paramValue = values.shift();
                                if (paramValue.indexOf("/") > -1) next = true;
                                params[parts[partIndex].replace(":", "")] = paramValue.split("/")[0];
                            }
                        }

                        if (next) continue;

                        return {
                            routeSchema: route.path,
                            name: route.name,
                            path: this.currentRoute,
                            params: params,
                            component: route.component
                        }
                    }
                }
                return {
                    routeSchema: '/not-found-404',
                    name: '404',
                    path: '/not-found-404',
                    params: [],
                    component: 'not-found-404'
                };
            },

            getRouteParam: function (name) {
                var oRegExp = new RegExp('[&]' + this.srcparam + '=([^&.]*)', 'i');
                var route = this.getRoute(decodeURIComponent(oRegExp.exec(window.location.href)[1]));
                if (null === route) {
                    return 404
                }
                if (undefined !== route && undefined !== route.params[name]) {
                    return route.params[name];
                }
            },

            dataLoadingDone: function (originName) {
                for (i in this.dataLoadingQueue) {
                    if (i === originName) {
                        delete this.dataLoadingQueue[originName];
                        this.appDataLoadingOff();
                        return;
                    }
                }
            },

            enqueueApiData: function (request) {
                if (this.dataLoadedIndex.indexOf(request.origin) !== -1) return
                if (undefined === this.dataLoadingQueue[request.origin]) {
                    this.dataLoadingQueue[request.getOrigin()] = request.getData();
                }
            },

            dataLoadingQueueEmpty: function () {
                var queue = this.dataLoadingQueue;
                var counter = 0;
                for (i in queue) {
                    counter++;
                }
                return counter === 0;
            },

            routeChangeStartCallback: function () {
                this.appDataLoading(true, this.tr('Components loading'));
            },

            appDataLoadingOn: function (msg) {
                this.appDataLoading(true, msg);
            },

            appDataLoadingOff: function () {
                if (this.dataLoadingQueueEmpty()) this.appDataLoading(false);
            },

            appLoadingOn: function (msg) {
                this.appLoading(true, msg);
            },

            appLoadingOff: function () {
                this.appLoading(false);
            },

            appLoading: function (state, msg) {
                var msg = undefined === msg ? false : msg;
                var state = undefined === state ? true : state;
                this.loading = state;
                if (!this.loading) msg = "";
                if (typeof msg === "object" && undefined !== msg.routeSchema) {
                    msg = this.tr("Components loading");
                }
                this.loadingMsg = msg;
            },

            appDataLoading: function (state) {
                var state = undefined === state ? true : state;
                this.dataLading = state;
            },

            getPrevPushedState: function () {
                return this.pushedStates[this.pushedStates.length - 1];
            },

            /*
            WIP: localisation endpoint
             */
            tr: function (value) {
                return value;
            }
        }
    });

})(Vue);