Store = new Vuex.Store({
    state: {
        VueComponentsLoaded: window['VueComponentsLoaded'],
        VueViewsLoaded: window['VueViewsLoaded'],
        gannerApiAccess: window['gannerApiAccess'],
        gannerApiToken: false,
        gannerApiTokenEndpoint: 'v1/auth/access-token'
    },

    mutations: {
        apiTokenRefresh: function (state, val) {
            if (undefined !== val) state.gannerApiToken = val;
        },

        VueViewLoadedAdd: function (state, val) {
            if (state.VueViewsLoaded.indexOf(val) === -1) state.VueViewsLoaded.push(val);
        },

        VueComponentsLoadedAdd: function (state, val) {
            if (state.VueComponentsLoaded.indexOf(val) === -1) state.VueComponentsLoaded.push(val);
        }
    },

    getters: {
        gannerApiTokenEndpoint: function (data) {
            return data.gannerApiTokenEndpoint;
        },
        gannerApiAccess: function (data) {
            return data.gannerApiAccess;
        },

        gannerApiToken: function (data) {
            return data.gannerApiToken;
        },

        VueViewLoadedG: function (data) {
            return data.VueViewsLoaded;
        },

        VueComponentsLoadedG: function (data) {
            return data.VueComponentsLoaded;
        }
    },

    actions: {}

});