(function () {
    /*
    data  = {
    key: localstorage key
    interval: interval in miliseconds
    success: callback on success
    }
     */
    var LocStorPreload = function (collectionName, type, data) {
        this.active = true;
        this.type = type || 'sessionStorage';
        this.data = data;
        this.collectionName = collectionName;
        this.cacheDisabled = geConfig.gannerc[this.type + 'CacheDisabled'];

        this.originName = this.data.key;
        this.moduleName = this.originName + 'Module';

        this.init = function () {
            if (undefined === window[this.type]) this.active = false;
            this.setInterval();
        }

        this.getStoredItemRequestData = function () {
            var localStorageMng = new window.LocStorMng(this.collectionName, this.type);
            return localStorageMng.getRequestData(this.key);
        }

        this.dataLoaded = function (payload) {
            this.inProgress = false;
            window[window.geConfig.gannerc.appVarName].registerStoreModule(this.moduleName, payload.data.data["Module-Data"]);
            if (!this.cacheDisabled) {
                var cacheMng = new window.LocStorMng(this.collectionName, "sessionStorage");
                if (cacheMng.active) {
                    var originName = payload.data.data["Ge-Data-Origin"];
                    var alive = payload.data.data["Cache-Alive"];
                    var newStorageItem = new window.LocStorItem(payload, alive);
                    cacheMng.add(originName, newStorageItem);
                }
            }
            window[window.geConfig.gannerc.appVarName].bus.$emit(this.originName + '-updated');
        }

        this.preload = function (obj) {
            if (window[window.geConfig.gannerc.appVarName].pausePreload) {
                console.log('preload paused');
                return;
            }

            var enabled = obj.enabled();
            var module = undefined !== Store.state[obj.moduleName] ? Store.state[obj.moduleName] : null;

            if (!enabled || (obj.cacheDisabled && !module)) {
                return;
            }

            if (module) {
                var dataLoadArgs = Object.assign(obj.data.loadDataArguments, {
                    callback: function (data) {
                        obj.dataLoaded(data);
                    }
                });

                var refreshStoreRequest = new apiDataRequest(obj.originName, dataLoadArgs);
                refreshStoreRequest.clean = function () {
                };

                false === refreshStoreRequest.apiToken ? refreshStoreRequest.aquireApiToken(refreshStoreRequest.makeDataRequest) : refreshStoreRequest.makeDataRequest();
            }

        }

        this.getHash = function () {
            return this.data.key;
        }

        this.startPreloading = function () {
            var hash = this.getHash();
            window.preloadIntervals = window.preloadIntervals || {};
            if (undefined === window.preloadIntervals[this.data.key]) {
                var intervalNo = setInterval(this.preload, this.data.interval, this);
                window.preloadIntervals[this.data.key] = intervalNo;
            }
        }

        this.setInterval = function () {
            this.interval = this.data.interval || 2000;
            this.key = this.data.key || false;
            this.success = this.data.success;
            this.enabled = this.data.enabled;

            if (this.key) {
                //preload process success function needs to call another setTimeout function. That architecture will mimic
                //setInterval behavior but load time will be a dynamic part of delay time between iterations
                this.startPreloading()
            }
        }


        return this.init(collectionName);
    };

    window.LocStorPreload = LocStorPreload;
})()