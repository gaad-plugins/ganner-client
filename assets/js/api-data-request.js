(function () {
    /*
        data format:
        {
            type: string
            headers: object({name: val ...})
        }

        silent mode - without Store integration
         */
    var apiDataRequest = function (origin, data, app, silentMode) {
        this.errors = [];
        this.response = null;
        this.done = false;
        this.app = app || window[window.geConfig.gannerc.appVarName];
        this.silentMode = silentMode || false;
        if (this.app) {
            this.apiAccess = this.app.$store.getters.gannerApiAccess;
            this.apiToken = this.app.$store.getters.gannerApiToken;
            this.authEndpoint = this.app.$store.getters.gannerApiTokenEndpoint;
        }
        this.origin = origin;
        this.collectionName = geConfig.gannerc.localStorageCollectionName || 'gc';
        this.data = data;
        this.type = undefined === data.type ? 'get' : data.type;
        this.headers = undefined === data.headers ? {} : data.headers;
        this.headers['Ge-Data-Origin'] = origin;
        if (undefined !== data['alive']) {
            this.headers['Cache-Alive'] = data['alive'];
        }

        this.send = function () {
            if (this.app.dataLoadedIndex.indexOf(this.origin) !== -1 && !this.silentMode) return;

            var cacheDisabled = geConfig.gannerc.sessionStorageCacheDisabled;
            if (!cacheDisabled) {
                //sessionStorage cache
                var cacheMng = new window.LocStorMng(this.collectionName, "sessionStorage");
                if (cacheMng.active) {
                    var storageItem = cacheMng.get(this.origin);
                    if (storageItem) {
                        var requestData = storageItem.getResponse();
                        var originName = requestData['Ge-Data-Origin'];
                        var moduleData = requestData['Module-Data'];
                        if (undefined !== Store) {
                            Store.registerModule(originName + "Module", {
                                state: moduleData
                            });
                        }
                        this.app.bus.$emit('dataLoadingDone', originName);
                        return;
                    }
                }
            }
            this.app.dataLoadedIndex.push(this.origin);
            false === this.apiToken ? this.aquireApiToken(this.makeDataRequest) : this.makeDataRequest();
        }

        this.addAuth = function () {
            if (this.apiToken) {
                this.headers['Authorization'] = "Bearer " + this.apiToken;
                return true;
            }
            return false;
        }

        this.makeDataRequest = function () {
            $this = this === window ? $this : this;
            if ($this.allowRequest()) {
                if ($this.apiToken) {
                    $this.headers['Authorization'] = "Bearer " + $this.apiToken;
                }
                $this.setLoadingMessage();
                axios[$this.type]($this.getRequestUrl(), {}, {headers: $this.headers})
                    .then($this.set)
                    .then($this.success)
                    .then($this.clean);
            }
        }

        this.setLoadingMessage = function () {
            if (undefined !== this.data.loadingMsg) {
                this.app.loadingMsg = this.data.loadingMsg;
            }
        }

        /*
        Not perfect technique to prevent doubled requests.
        * */
        this.allowRequest = function () {
            var _return = false;
            var hash = JSON.stringify(this.data).hashCode();
            var app = window[window.geConfig.gannerc.appVarName];
            var requestBlockPeriod = geConfig.gannerc.requestBlockPeriod || 2000;
            var now = +new Date();

            if (undefined === app.lastRequestHash[this.origin]) {
                app.lastRequestHash[this.origin] = {
                    time: now, hash: hash
                };
                _return = true;
            } else {
                var time = app.lastRequestHash[this.origin].time;
                var delay = now - time;
                _return = delay >= requestBlockPeriod;
                if (_return) {
                    app.lastRequestHash[this.origin] = {
                        time: now, hash: hash
                    };
                }

            }

            return _return;
        }

        this.aquireApiToken = function (callbackFunction) {
            if (undefined !== this.apiAccess.uid && undefined !== this.apiAccess.domain) {
                var headers = {
                    uid: this.apiAccess.uid,
                    domain: this.apiAccess.domain,
                    u: 'self',
                    p: 'auth'
                };
            } else {
                var headers = {
                    u: this.apiAccess.user,
                    p: this.apiAccess.pass
                };
            }

            $this = this;
            axios.post(this.getAuthUrl(), {}, {headers: headers})
                .then(function (data) {
                    if (200 === data.status && "success" === data.data.status) {
                        $this.apiToken = data.data.data.access_token;
                        $this.app.$store.commit('apiTokenRefresh', $this.apiToken);
                    }
                    return data;
                })
                .then(undefined !== callbackFunction ? callbackFunction : function () {
                });
        }

        this.clean = function (originName) {
            this.app.bus.$emit('dataLoadingDone', originName);
        }

        this.actionPermissionDeniedError = function (data) {
            var app = window[window.geConfig.gannerc.appVarName];
            var originIndex = app.dataLoadedIndex.indexOf($this.origin);
            if (originIndex !== -1) {
                delete app.dataLoadedIndex[originIndex];
                var length = 0;
                app.dataLoadedIndex.filter(function (el) {
                    el != null ? length++ : false;
                    return el != null;
                });
                app.dataLoadedIndex.length = length;
            }
            if (undefined !== app.dataLoadingQueue[$this.origin]) {
                delete app.dataLoadingQueue[$this.origin];
            }

            var errorMessage = data.data.data["Module-Data"].message;
            if (undefined !== errorMessage) {
                app.bus.$emit('notification-push', {
                    label: 'action-permission-denied-msg',
                    type: 'error',
                    msg: errorMessage
                });
                app.bus.$emit('bucket-add-task-cancel');
                app.bus.$emit('bucket-add-cancel');
            }

        }

        this.authOutdatedError = function (data) {
            var app = window[window.geConfig.gannerc.appVarName];
            var originIndex = app.dataLoadedIndex.indexOf($this.origin);
            if (originIndex !== -1) {
                delete app.dataLoadedIndex[originIndex];
                var length = 0;
                app.dataLoadedIndex.filter(function (el) {
                    el != null ? length++ : false;
                    return el != null;
                });
                app.dataLoadedIndex.length = length;
            }

            this.aquireApiToken(this.makeDataRequest);
        }

        this.getEssentialData = function () {
            var headersWList = ['Ge-Data-Origin'];
            var headers = {};
            for (i in this.headers) {
                if (headersWList.indexOf(i) !== -1) {
                    headers[i] = this.headers[i];
                }
            }

            return {
                type: this.type,
                data: this.data,
                headers: headers
            }
        }

        this.success = function (data) {
            //this if part is for handling silent mode requests that only updates the local storage cache
            if ("VueComponent" === $this.__proto__.constructor.name && undefined !== $apiDataRequest) {
                $this = $apiDataRequest;
                delete $apiDataRequest;
            }

            if (200 === data.status && "error" === data.data.status) {
                if (403 === data.data.code) {
                    $this.actionPermissionDeniedError(data);
                    return;
                }
                if (430 === data.data.code) {
                    $this.authOutdatedError(data);
                    return;
                }
            }
            if (200 === data.status && "success" === data.data.status) {
                var requestData = data.data.data;
                var originName = requestData['Ge-Data-Origin'];
                var moduleData = requestData['Module-Data'];
                var alive = requestData['Cache-Alive'];
                if (undefined !== Store && !this.silentMode) {
                    window[window.geConfig.gannerc.appVarName].registerStoreModule(originName, moduleData);
                }

                var cacheDisabled = geConfig.gannerc.sessionStorageCacheDisabled;
                if (!cacheDisabled) {
                    //local storage cache handling
                    var cacheMng = new window.LocStorMng($this.collectionName, "sessionStorage");
                    if (cacheMng.active) {
                        cacheMng.add(originName, new window.LocStorItem(data, alive), $this);
                    }
                }

                typeof $this.data.callback === "function" ? $this.data.callback(data) : false;
            }
            return originName;
        }

        this.set = function (data) {
            if (200 === data.status) {
                $this.response = data;
                $this.done = true;
            }
            return data;
        }

        this.getRequestUrl = function () {
            return window.location.protocol + '//' + window.location.host + '/' + geConfig.rest.routePrefix + this.data.endpoint;
        }
        this.getAuthUrl = function () {
            return window.location.protocol + '//' + window.location.host + '/' + geConfig.rest.routePrefix + this.authEndpoint;
        }

        this.getOrigin = function () {
            return this.origin;
        }

        this.getData = function () {
            return this.data;
        }
    }

    window.apiDataRequest = apiDataRequest;
})()