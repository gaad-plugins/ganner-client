#! /bin/bash
# GPL 3 or newer

set -x
set -e
npm i

cp /tmp/assets-compile.js /var/www/html/
#chown gaad /var/www/html/assets-compile.js
#chgrp gaad /var/www/html/assets-compile.js

#chown gaad /var/www/html/package-lock.json
#chgrp gaad /var/www/html/package-lock.json

cp /tmp/package.json /var/www/html/
#chown gaad /var/www/html/package.json
#chgrp gaad /var/www/html/package.json

cp -r /tmp/repo /var/www/html/
#chown -R gaad /var/www/html/repo/gassets-comp
#chgrp -R gaad /var/www/html/repo/gassets-comp

node /var/www/html/assets-compile.js