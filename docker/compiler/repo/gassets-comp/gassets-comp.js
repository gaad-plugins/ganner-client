const chokidar = require('chokidar')
const util = require('util')
const fs = require('fs')
const sass = require('node-sass')
const minify = require('@node-minify/core')
const cleanCSS = require('@node-minify/clean-css')
const clc = require('cli-color')
const autoprefixer = require('autoprefixer')
const postcss = require('postcss')
const logSymbols = require('log-symbols');
const path = require('path');
let directories = [];

function getDirectories(path, suffix) {
    let suffix_ = undefined !== suffix ? suffix : '';
    let dir = fs.readdirSync(path).filter(function (file) {
        return fs.statSync(path + '/' + file).isDirectory();
    });
    let fullPath = path + suffix_
    fullPath = fullPath.replace("//", "/")
    if (fs.existsSync(fullPath)) directories.push(fullPath);

    if (0 !== dir.length) {
        for (let i = 0; i < dir.length; i++) {
            getDirectories(path + "/" + dir[i], suffix_);
        }
    }
}

let options = {
    minify: true
}

let compileVueComponentSass = function (path, options) {
    let steps = ['compile', 'autoprefixer', 'minify', 'output']
    let output = []
    output['success'] = logSymbols.success + " File " + clc.green(path.split('ganner-client')[1]) + " has been";

    function updateOutput(sPart, group) {
        let group_ = undefined !== group ? group : 'success';
        output[group_] += sPart
    }

    function getOutput() {
        return output['success']
    }

    let options_ = undefined === options ? {} : options;
    let defaults = {
        minify: true,
        autoprefixer: true
    }
    let opt = Object.assign(defaults, options_);
    let sass = require('node-sass');
    let cssFile = path.replace(".scss", ".css");

    function getSassHeaderData() {
        let sDir = '/var/www/html/assets/scss/gendpoints/'
        let aFiles = ['colors', 'variables', 'mixins']
        let aConcat = []
        for (let i = 0; i < aFiles.length; i++) {
            aConcat.push(fs.readFileSync(sDir + aFiles[i] + '.scss', 'utf8'))
        }
        return aConcat.join("\n");
    }

    function compileStep() {
        let sassHeaderData = getSassHeaderData()
        let sassData = fs.readFileSync(path, 'utf8')

        let result = sass.renderSync({
            data: sassHeaderData + sassData
        });

        write(result.css.toString());
        updateOutput(" compiled");
    }

    function outputStep(opt) {
        console.log(getOutput());
    }

    function autoprefixerStep(opt) {
        if (opt.autoprefixer) {
            let cssData = fs.readFileSync(cssFile, 'utf8');
            let postcssOptions = {
                from: undefined
            };
            postcss([autoprefixer]).process(cssData).then(result => {
                result.warnings().forEach(warn => {
                    console.warn(warn.toString())
                })
                write(result.css)
                updateOutput(", autoprefixed");
            })
        }
    }

    function minifyStep(opt) {
        if (opt.minify) {
            let cssMinFile = cssFile.replace(".css", ".min.css")
            minify({
                compressor: cleanCSS,
                input: cssFile,
                output: cssMinFile,
                callback: function (err, min) {
                    if (null == err) {
                        updateOutput(", minified");
                        nextStep()
                    }
                }
            });
        }
    }

    function nextStep() {
        if (steps.length > 0) {
            let step = steps.shift() + "Step";
            let evalStr = step + '(opt)'
            try {
                eval(evalStr);
            } catch (e) {
                console.log(e);
            }
        }
    }

    function write(data) {
        fs.writeFileSync(cssFile, data);
        nextStep()
    }

    nextStep();
}


function compileEverything() {
    let sDir = __dirname.split('ganner-client')[0] + 'ganner-client/gendpoints/'
    getDirectories(sDir, '/styles.css');
    if (directories.length) {
        for (const directoriesKey in directories) {
            let path = directories[directoriesKey];
            compileVueComponentSass(path, options);
        }
    }
}

module.exports = {

    getDirectories(path, suffix) {
        getDirectories(path, suffix);
        return directories;
    },

    watcher(path) {
        return chokidar.watch(path, {
            ignored: /^[\.]{1,2}|.*\.php|.*\.js|.*\.css$/mg,
            persistent: true
        })
    },

    watch(path, options) {
        let sGlobalDir = path.split('ganner-client')[0] + '/ganner-client/assets/scss/gendpoints/'
        let global = this.watcher(sGlobalDir);
        if (global) {
            global
                .on('change', function (path) {
                    compileEverything()
                })
        }
        let w = this.watcher(path);
        if (w) {
            w
                .on('add', function (path) {
                    compileVueComponentSass(path, options);
                })
                .on('change', function (path) {
                    compileVueComponentSass(path, options);
                })
                .on('unlink', function (path) {
                    console.log('File', path, 'has been removed');
                })
                .on('error', function (error) {
                    console.error('Error happened', error);
                })
        }

    },

}