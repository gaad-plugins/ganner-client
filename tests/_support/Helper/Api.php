<?php

namespace Helper;

// here you can define custom actions
// all public methods declared in helper class will be available in $I

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\Setup;

class Api extends \Codeception\Module
{
    const DB_NAME = 'wpdocker';
    const DB_USER = 'root';
    const DB_PASSWORD = 'root';
    const DB_HOST = '127.0.0.1:33064';
    const ENV = 'dev';
    const GANNER_DIR = __DIR__ . '/../../../';
    const Entities = ['Project', 'Bucket', 'Task', 'Comment'];
    const EntitiesMap = [
        'Project' => "\Gaad\Ganner\Entity\Project",
        'Bucket' => "\Gaad\Ganner\Entity\Bucket",
        'Task' => "\Gaad\Ganner\Entity\Task",
        'Comment' => "\Gaad\Ganner\Entity\Comment",
    ];
    const EntitiesTablesMap = [
        'Project' => "ganner_project",
        'Bucket' => "ganner_bucket",
        'Task' => "ganner_task",
        'Comment' => "ganner_comment",
    ];

    public function _beforeSuite($settings = array())
    {
        $this->createEntityManager();

    }

// HOOK: before each suite

    public function createEntityManager()
    {
        $GLOBALS['oGAEntityManager'] = EntityManager::create([
            'driver' => 'pdo_mysql',
            'host' => self::DB_HOST,
            'user' => self::DB_USER,
            'password' => self::DB_PASSWORD,
            'dbname' => self::DB_NAME
        ], Setup::createAnnotationMetadataConfiguration(array(self::GANNER_DIR . "inc/class/Entity"), self::ENV === 'dev' ?? false, null, null, false));

        global $oGAEntityManager;
        $oGAEntityManager
            ->getConnection()
            ->getDatabasePlatform()
            ->registerDoctrineTypeMapping('enum', 'string');

        return $oGAEntityManager;
    }

    public function removeAllEntities()
    {
        global $oGAEntityManager;


        foreach ($this::Entities as $sEntityName) {
            $RAW_QUERY = "TRUNCATE {$this::EntitiesTablesMap[$sEntityName]};";
            $statement = $oGAEntityManager->getConnection()->prepare($RAW_QUERY);
            $statement->execute();
        }
    }
}
