<?php

/*
 * This file is part of Mustache.php.
 *
 * (c) 2010-2017 Justin Hileman
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

class I18n
{
    // Variable to be interpolated
    private static $dictionary = array(
        'Hello.'                 => 'Hola.',
        'My name is {{ name }}.' => 'Me llamo {{ name }}.',
    );

    // Add a {{#__}} lambda for i18n
    public $name = 'Bob';

    // A *very* small i18n dictionary :)
    public $__ = array(__CLASS__, '__trans');

    public static function __trans($text)
    {
        return isset(self::$dictionary[$text]) ? self::$dictionary[$text] : $text;
    }
}
