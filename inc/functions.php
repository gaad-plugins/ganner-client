<?php

namespace Gaad\GunnerClient\Core\Filters;

use Gaad\Gendpoints\Config\Config;

if (!function_exists('\Gaad\GunnerClient\Core\Filters\vueAppScriptDefer')) {
    function vueAppScriptDefer($tag, $handle, $src)
    {
        if (in_array($handle, ['vue-app'])) {
            return '<script src="' . $src . '" defer="defer" type="text/javascript"></script>' . "\n";
        }
        return $tag;
    }
}

if (!function_exists('\Gaad\GunnerClient\Core\Filters\vueTemplatesLoading')) {
    function vueTemplatesLoading()
    {
        global $oGACVueComponents;
        $oGACVueComponents->render();
    }
}

if (!function_exists('\Gaad\GunnerClient\Core\Filters\buildStylesQueue')) {
    function buildStylesQueue()
    {
        wp_enqueue_style('bootstrap-css', "https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css");
    }
}

if (!function_exists('\Gaad\GunnerClient\Core\Filters\buildAssetsQueue')) {
    function buildAssetsQueue()
    {
        wp_enqueue_script('vue-app-routes', __GANNERC_URI__ . '/assets/js/routes.js', [], FALSE, TRUE);
        wp_enqueue_script('axios', 'https://unpkg.com/axios/dist/axios.min.js', FALSE, FALSE, TRUE);
        wp_enqueue_script('vue', 'https://cdn.jsdelivr.net/npm/vue/dist/vue.js', FALSE, FALSE, TRUE);
        wp_enqueue_script('vue-x', 'https://unpkg.com/vuex', ['vue'], FALSE, TRUE);
        wp_enqueue_script('vue-router', 'https://unpkg.com/vue-router/dist/vue-router.js', ['vue'], FALSE, TRUE);
        wp_enqueue_script('jquery-slim', 'https://code.jquery.com/jquery-3.3.1.js', [], FALSE, TRUE);
        wp_enqueue_script('popper-js', 'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js', ['jquery-slim'], FALSE, TRUE);
        wp_enqueue_script('bootstrap-js', 'https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js', ['jquery-slim', 'popper-js'], FALSE, TRUE);
        wp_enqueue_script('font-awesome-js', 'https://kit.fontawesome.com/383cbb7a03.js', [], FALSE, TRUE);
        wp_enqueue_script('ge-tinymce', 'https://cdn.tiny.cloud/1/e5q7q5lxxbzonp9x3egobpy366ozh7srkmbs3ica42xqd9yy/tinymce/5/tinymce.min.js', [], FALSE, FALSE);
        wp_enqueue_script('sortable-js', '//cdn.jsdelivr.net/npm/sortablejs@1.8.4/Sortable.min.js', [], FALSE, TRUE);
        wp_enqueue_script('vue-draggable-js', '//cdnjs.cloudflare.com/ajax/libs/Vue.Draggable/2.20.0/vuedraggable.umd.min.js', ['vue'], FALSE, TRUE);
        wp_enqueue_script('vue-app-store', __GANNERC_URI__ . '/assets/js/store.js', ['vue'], FALSE, TRUE);
        wp_enqueue_script('vue-validators', __GANNERC_URI__ . '/node_modules/vuelidate/dist/validators.min.js', [], FALSE, FALSE);
        wp_enqueue_script('vue-vuelidate', __GANNERC_URI__ . '/node_modules/vuelidate/dist/vuelidate.min.js', ['vue-validators'], FALSE, TRUE);
        wp_enqueue_script('vue-app-translations', __GANNERC_URI__ . '/assets/js/app-translations-en.js', false, FALSE, TRUE);
        wp_enqueue_script('vue-app', __GANNERC_URI__ . '/assets/js/app.js', ['vue-app-translations', 'vue', 'vue-app-store', 'vue-app-router', 'vue-vuelidate'], FALSE, TRUE);
        wp_enqueue_script('api-data-request', __GANNERC_URI__ . '/assets/js/api-data-request.js', [], FALSE, TRUE);
        wp_enqueue_script('app-loc-storage-item', __GANNERC_URI__ . '/assets/js/loc-storage-item.js', [], FALSE, TRUE);
        wp_enqueue_script('app-loc-storage-mng', __GANNERC_URI__ . '/assets/js/loc-storage-mng.js', ['app-loc-storage-stats', 'app-loc-storage-item'], FALSE, TRUE);
        wp_enqueue_script('app-loc-storage-preload', __GANNERC_URI__ . '/assets/js/loc-storage-preload.js', ['app-loc-storage-stats', 'app-loc-storage-item'], FALSE, TRUE);
        wp_enqueue_script('app-loc-storage-stats', __GANNERC_URI__ . '/assets/js/loc-storage-stats.js', [], FALSE, TRUE);
        wp_enqueue_script('fonts-awesome-kit', 'https://kit.fontawesome.com/383cbb7a03.js', [], FALSE, TRUE);
    }
}

/*
 * Adds necessary variables to url during invoking Ganner client admin panel options page
 */
if (!function_exists('\Gaad\GunnerClient\Core\Filters\gannercOptionsPageRedirect')) {
    function gannercOptionsPageRedirect()
    {
        global $geConfig;
        $sOptionsPageSlug = $geConfig->get('optionsPageSlug', 'gannerc');
        $sRouteVarName = $geConfig->get('routeVarName', 'gannerc');
        if (isset($_GET['page']) && $sOptionsPageSlug === $_GET['page']) {
            if (!isset($_GET[$sRouteVarName])) {
                $sLocation = '//' . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"] . '&' . $sRouteVarName . '=/';
                header("Location: {$sLocation}");
            }
        }
    }
}

/*
 * Ganner admin options page content generator. Basically it is an Vue app holder container and basic app template
 */
if (!function_exists('\Gaad\GunnerClient\Core\Filters\gannercOptionsPageContent')) {
    function gannercOptionsPageContent()
    {
        global $geConfig;
        $sRouteVarName = $geConfig->get('routeVarName', 'gannerc');
        ?>
    <div id="gannerc" srcparam="<?php echo $sRouteVarName ?>">
        <main-menu></main-menu>
        <notifications></notifications>
        <loading v-if="loading || manifestLoading"
                 :loading-msg="true"
        ></loading>
        <router v-else :routes="groutes" :srcparam="'<?php echo $sRouteVarName ?>'" ref="router"></router>

        </div><?php
    }
}

/*
 * WP Admin Option page declaration
 * */
if (!function_exists('\Gaad\GunnerClient\Core\Filters\OptionsPage')) {
    function OptionsPage()
    {
        global $geConfig, $aGannerOptionsPageParams;
        $sMenuTitle = $geConfig->get('optionsPageMenuTitle', 'gannerc');
        $sOptionsPageTitle = $geConfig->get('optionsPageTitle', 'gannerc');
        $sOptionsPageSlug = $geConfig->get('optionsPageSlug', 'gannerc');
        \add_menu_page($sOptionsPageTitle, $sMenuTitle, 'manage_options', $sOptionsPageSlug, '\Gaad\GunnerClient\Core\Filters\gannercOptionsPageContent');
        \add_filter('menu_page_capability_' . $sOptionsPageSlug, function () {
            return 'edit_posts';
        });
    }
}

/*
 * Returns array with configuration files directories
 * */
if (!function_exists('\Gaad\GunnerClient\Core\Filters\configDirectories')) {
    function configDirectories($input)
    {
        $input[] = __GANNERC_DIR__ . '/config';
        return $input;
    }
}

/*
 * Returns array with gendpoints files directories
 * */
if (!function_exists('\Gaad\GunnerClient\Core\Filters\endpointsDirectories')) {
    function endpointsDirectories($input)
    {
        $input[] = __GANNERC_DIR__ . '/gendpoints';
        return $input;
    }
}

/*
 * Transport Ganner API access data to frontend application (Ganner Client)
 * */
if (!function_exists('\Gaad\GunnerClient\Core\Filters\backendDataTransport')) {
    function backendDataTransport()
    {
        global $geConfig;
        $iCurrentUserID = get_current_user_id();
        $oCurrentUser = new \WP_User($iCurrentUserID);

        $sAuthHost = 'http://';
        if ((!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off') || $_SERVER['SERVER_PORT'] == 443) {
            $sAuthHost = 'https://';
        }

        $sAuthHost .= $_SERVER['HTTP_HOST'];
        if (!empty($oCurrentUser->roles)) {
            $aGannerApiAccess = [
                'uid' => $iCurrentUserID,
                'domain' => $sAuthHost
            ];
            $aGunnerCapsWhiteList = $geConfig->get("caps", "all-caps");
            $geUserCaps = $oCurrentUser->allcaps;
            $geUserRoles = $oCurrentUser->roles;
            $aFilteredCaps = [];
            foreach ($geUserCaps as $capName => $enabled) {
                if ($enabled) {
                    if (in_array('administrator', $geUserRoles) || in_array($capName, $aGunnerCapsWhiteList)) {
                        $aFilteredCaps[] = $capName;
                    }

                    //views support
                    preg_match_all('/.*-view/m', $capName, $matches, PREG_SET_ORDER, 0);
                    if (!empty($matches)) {
                        $aFilteredCaps[] = $capName;
                    }
                    unset($matches);

                }
            }
            $geUserRoles = array_values($geUserRoles);
        }
        ?>
        <script id="gannerApiAccess" type="application/javascript">
            window['geUserCaps'] = {
                'caps': <?php echo json_encode($aFilteredCaps, true); ?>,
                'roles': <?php echo json_encode($geUserRoles, true); ?>
            };
            window['geConfig'] = <?php echo $geConfig->getJSON(); ?>;
            window['gannerApiAccess'] = <?php echo json_encode($aGannerApiAccess); ?>;
        </script>
        <?php
    }
}