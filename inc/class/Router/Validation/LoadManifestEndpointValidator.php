<?php
/**
 * Validate ganner/project/create request
 */

namespace Gaad\Gendpoints\Router\Validation;

use Gaad\GannerClient\Handlers\VueComponentsManifest;
use Gaad\Gendpoints\Interfaces\RouteValidationHandler;

class LoadManifestEndpointValidator extends GenericEndpointValidator implements RouteValidationHandler
{

    const aHeaders = ['Route'];

    public function validate__Route($value, array $aHeaders): void
    {
        global $oGACVueComponents;
        $oManifest = new VueComponentsManifest($value, $oGACVueComponents);
        !$oManifest->isValid() ? $this->reportError(\__("Vue app route doesn't exists"), 4444) : false;
    }

    /**
     * @return array
     */
    public function getAHeaders(): array
    {
        return array_merge(parent::aHeaders, $this::aHeaders);
    }
}