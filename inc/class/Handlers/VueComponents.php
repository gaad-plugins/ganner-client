<?php

namespace Gaad\GannerClient\Handlers;


use Gaad\Command\genericCommand;
use \Gaad\Gendpoints\Config\Config;
use Gaad\Gendpoints\GEndpoint;
use \Gaad\Gendpoints\Logger;
use PHPUnit\Exception;
use function Gaad\Gendpoints\Core\Functions\registerRestRoutes as registerRestRoutes;

class VueComponents extends \WP_Dependencies
{
    private $sFile = 'vue-components.yaml';
    private $aComponentsData = [];
    private $oLogger;
    private $aTemplates;
    private $aStyles;
    private $aComponents;

    /**
     * vueTemplates constructor.
     * @param array $aConfigDirectories
     * @param null $sFile
     * @throws \Exception
     */
    public function __construct(array $aConfigDirectories, $sFile = NULL)
    {
        $this->sFile = $sFile ?? $this->sFile;
        $this->aComponentsData = (new Config($this->sFile, $aConfigDirectories))->get();
        $this->oLogger = new Logger(null, "");
    }

    public function delComponent(string $sComponentName): bool
    {
        if (array_key_exists($sComponentName, $this->aComponentsData)) {
            unset($this->aComponentsData[$sComponentName]);
            return true;
        }
        return false;
    }

    public function addComponent(VueComponent $oComponent)
    {
        $this->aComponents[] = $oComponent;
        $aComponentData = [
            'endpoint' => [],
            'headers' => $oComponent->getHeaders(),
            'params' => $oComponent->getParams(),
            'version' => $oComponent->getVersion(),
            'depends' => $oComponent->getDepends(),
        ];
        //@TODO Implement naming fix to remove this need for map. Component should be the named application.
        $aMap = ['application' => 'component', 'template' => 'template'];
        //@TODO Do some research why this relative path starts with / and fix it. Relative paths shouldn't start with /.
        $aComponentData['endpoint'] [$aMap[$oComponent->getType()]] = "/" . $oComponent::TYPE2DIR[$oComponent->getType()] . "/" . $oComponent->getContext() . "/" . $oComponent->getName();
        if (array_key_exists($oComponent->getName(), $this->aComponentsData)) {
            //merge
            $this->aComponentsData[$oComponent->getName()]['endpoint'][$aMap[$oComponent->getType()]] = "/" . $oComponent::TYPE2DIR[$oComponent->getType()] . "/" . $oComponent->getContext() . "/" . $oComponent->getName();
        } else {
            $this->aComponentsData[$oComponent->getName()] = $aComponentData;
        }

    }

    public function toYAML()
    {
        return yaml_emit(['vue-components' => $this->aComponentsData]);
    }

    public function render()
    {
        $this->fetchComponents();
        $this->componentsToHtml();
        $this->templatesStylesToHtml();
        echo $this->templatesToHtml();
        echo $this->componentsSummaryToFrontend();
    }

    public function fetchComponents(?string $sRoute = NULL, ?array $aIgnore = NULL)
    {
        global $geConfig;
        $this->registerComponents();
        $sRouteVarName = $geConfig->get('routeVarName', 'gannerc');
        $oManifest = new VueComponentsManifest(!is_null($sRoute) ? $sRoute : $_GET[$sRouteVarName], $this);
        $this->enqueueComponents($oManifest);
        $this->do_items(false, false);

        registerRestRoutes();
        foreach ($this->done as $handle) {
            if (in_array($handle, is_array($aIgnore) ? $aIgnore : [])) continue;
            try {
                $oSrc = json_decode($this->registered[$handle]->src);
                $oTemplate = new GEndpoint($oSrc->template);
                $this->aComponents[$handle] = new GEndpoint($oSrc->component);
                $this->aStyles[$handle] = $oTemplate->getStaticStylesUrl();
                $this->addResource($handle, $oTemplate->output(), "template");
            } catch (Exception $e) {
                //@TODO log something here
            }
        }
    }


    public function getComponentData(string $sName): ?array
    {
        $aComponentsData = $this->getAComponentsData();
        if (array_key_exists($sName, $aComponentsData)) {
            return $aComponentsData[$sName];
        }
        return null;
    }

    public function registerComponents()
    {
        foreach ($this->getAComponentsData() as $slug => $v) {
            $this->add(
                $slug,
                \json_encode([
                    "template" => $this->getUrl($v, "template"),
                    "style" => "dupa",
                    "component" => $this->getUrl($v, "component")
                ]),
                $v["depends"]
            );
        }
    }

    /**
     * @return array|mixed|string
     */
    public function getAComponentsData()
    {
        return $this->aComponentsData;
    }

    /**
     * @param array|mixed|string $aComponentsData
     */
    public function setAComponentsData($aComponentsData): void
    {
        $this->aComponentsData = $aComponentsData;
    }

    private function getUrl(array $aData, string $context = "component")
    {
        global $geConfig;
        return str_replace(["//"], ["/"], $geConfig->get("routePrefix", "rest") . "/v" . $aData["version"] . "/" . $aData["endpoint"][$context]);
    }

    public function enqueueComponents(VueComponentsManifest $oManifest = NULL)
    {
        $aRegistered = $oManifest ? $oManifest->getComponets() : $this->registered;
        foreach ($aRegistered as $handle => $item) {
            $this->enqueue($handle);
        }
    }

    /**
     * @param string $handle
     * @param string $sContent
     */
    public function addResource(string $handle, string $sContent, string $sContext): void
    {
        $sName = "a" . ucwords($sContext) . "s";
        if (property_exists($this, $sName))
            $this->$sName[$handle] = $sContent;
    }

    /**
     * @return mixed
     */
    public function getATemplates()
    {
        return $this->aTemplates;
    }

    /**
     * @param mixed $aTemplates
     */
    public function setATemplates($aTemplates): void
    {
        $this->aTemplates = $aTemplates;
    }

    /**
     * @return string
     */
    public function getSFile(): string
    {
        return $this->sFile;
    }

    /**
     * @param string $sFile
     */
    public function setSFile(string $sFile): void
    {
        $this->sFile = $sFile;
    }

    /**
     * @return Logger
     */
    public function getOLogger(): Logger
    {
        return $this->oLogger;
    }

    /**
     * @param Logger $oLogger
     */
    public function setOLogger(Logger $oLogger): void
    {
        $this->oLogger = $oLogger;
    }

    /**
     * @return mixed
     */
    public function getAComponents()
    {
        return $this->aComponents;
    }

    /**
     * @param mixed $aComponents
     */
    public function setAComponents($aComponents): void
    {
        $this->aComponents = $aComponents;
    }

    public function templatesToHtml()
    {
        global $geConfig;
        $sVueTemplateHandlePrefix = $geConfig->get("vueTemplateHandlePrefix", "gannerc");
        $aHtml = [];
        if (!empty($this->getATemplates())) {
            foreach ($this->getATemplates() as $handle => $sContent) {
                $sID = $sVueTemplateHandlePrefix . $handle;
                $aHtml[] = "<script type=\"template/javascript\" id=\"" . $sID . "\">" . $sContent . "</script>";
            }
        }
        return !empty($aHtml) ? implode("\n", $aHtml) : "";
    }

    private function templatesStylesToHtml(): void
    {
        if (!empty($this->getAStyles())) {
            foreach ($this->getAStyles() as $handle => $oCssStylePath) {
                wp_enqueue_style('vue-app-component-styles-' . $handle, $oCssStylePath, []);
            }
        }
    }

    private function componentsToHtml(): void
    {
        if (!empty($this->getAComponents())) {
            $vueAppComponents = [];
            foreach ($this->getAComponents() as $handle => $oGEndpoint) {
                $componentHandle = 'vue-app-component-' . $handle;
                wp_enqueue_script($componentHandle, $oGEndpoint->getStaticUrl(), ['vue-app-translations'], FALSE, true);
                $vueAppComponents[] = $componentHandle;
            }
            $deps = array_merge(['vue-router'], $vueAppComponents);

            wp_enqueue_script('vue-app-router', __GANNERC_URI__ . '/assets/js/router.js', $deps, FALSE, TRUE);
        }
    }

    /**
     * @return mixed
     */
    public function getAStyles()
    {
        return $this->aStyles;
    }

    private function __clone()
    {
    }

    private function __wakeup()
    {
    }


    /**
     * Creates a js variable with all components handles that were loaded to the view
     * @return false|string
     */
    private function componentsSummaryToFrontend()
    {
        global $oGACVueComponents, $geConfig;
        $sRouteVarName = $geConfig->get('routeVarName', 'gannerc');
        $loadedFiles = [];
        foreach ($this->getAComponents() as $handle => $oGEndpoint) {
            $loadedFiles[] = $handle;
        }
        ob_start();
        $oManifest = new VueComponentsManifest($_GET[$sRouteVarName], $oGACVueComponents);
        ?>
        <script id="VueComponentsLoaded" type="application/javascript">
            window['VueComponentsLoaded'] = <?php echo \json_encode($loadedFiles); ?>;
            window['VueViewsLoaded'] = ['<?php echo $oManifest->getRouteSchema() ?>'];
        </script>
        <?php
        return ob_get_clean();
    }

}