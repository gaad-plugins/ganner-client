<?php

namespace Gaad\GannerClient\Handlers;

class TemplateVueComponentCreator extends VueComponentGenericCreator
{

    public function create(string $sVueComponentType)
    {
        $aStatus[] = parent::create($sVueComponentType);
        $aStatus[] = parent::createFile('styles', ["modelExt" => "scss"]);

        return !in_array(false, $aStatus);

    }


    /**
     * @return array
     */
    public function getErrors(): array
    {
        return $this->errors;
    }

    /**
     * @return mixed
     */
    public function getVueComponent(): ?VueComponent
    {
        return $this->vueComponent;
    }

    /**
     * @return mixed
     */
    public function getBasePath()
    {
        return $this->basePath;
    }

    /**
     * @return string
     */
    public function getVueComponentType(): string
    {
        return $this->vueComponentType;
    }

    /**
     * @param string $vueComponentType
     */
    public function setVueComponentType(string $vueComponentType): void
    {
        $this->vueComponentType = $vueComponentType;
    }


}