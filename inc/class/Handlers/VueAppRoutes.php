<?php

namespace Gaad\GannerClient\Handlers;


use Gaad\Command\genericCommand;
use \Gaad\Gendpoints\Config\Config;
use Gaad\Gendpoints\GEndpoint;
use \Gaad\Gendpoints\Logger;
use PHPUnit\Exception;
use function Gaad\Gendpoints\Core\Functions\registerRestRoutes as registerRestRoutes;

class VueAppRoutes
{
    private $sFile = 'vue-routes.yaml';
    private $aRoutesData = [];
    private $oLogger;
    private $aRoutes;

    /**
     * vueTemplates constructor.
     * @param array $aConfigDirectories
     * @param null $sFile
     * @throws \Exception
     */
    public function __construct(array $aConfigDirectories, $sFile = NULL)
    {
        $this->sFile = $sFile ?? $this->sFile;
        $this->aRoutesData = (new Config($this->sFile, $aConfigDirectories))->get();
        $this->oLogger = new Logger(null, "");
    }

    public function delRoute(string $sRouteName): bool
    {
        if (array_key_exists($sRouteName, $this->aRoutesData)) {
            unset($this->aRoutesData[$sRouteName]);
            return true;
        }
        return false;
    }

    public function addRoute(VueRoute $oRoute)
    {
        $this->aRoutes[] = $oRoute;
        $aRouteData = [
            'endpoint' => [],
            'headers' => $oRoute->getHeaders(),
            'params' => $oRoute->getParams(),
            'version' => $oRoute->getVersion(),
            'depends' => $oRoute->getDepends(),
        ];
        //@TODO Implement naming fix to remove this need for map. Route should be the named application.
        $aMap = ['application' => 'component', 'template' => 'template'];
        //@TODO Do some research why this relative path starts with / and fix it. Relative paths shouldn't start with /.
        $aRouteData['endpoint'] [$aMap[$oRoute->getType()]] = "/" . $oRoute::TYPE2DIR[$oRoute->getType()] . "/" . $oRoute->getContext() . "/" . $oRoute->getName();
        if (array_key_exists($oRoute->getName(), $this->aRoutesData)) {
            //merge
            $this->aRoutesData[$oRoute->getName()]['endpoint'][$aMap[$oRoute->getType()]] = "/" . $oRoute::TYPE2DIR[$oRoute->getType()] . "/" . $oRoute->getContext() . "/" . $oRoute->getName();
        } else {
            $this->aRoutesData[$oRoute->getName()] = $aRouteData;
        }

    }

    public function toYAML()
    {
        return yaml_emit(['vue-components' => $this->aRoutesData]);
    }

    public function render()
    {
        $this->fetchRoutes();
    }

    public function fetchRoutes()
    {
        $this->registerRoutes();
        $this->enqueueRoutes();
        $this->do_items(false, false);

        registerRestRoutes();
        foreach ($this->done as $handle) {
            try {
                $oSrc = json_decode($this->registered[$handle]->src);
                $oTemplate = new GEndpoint($oSrc->template);
                $this->aRoutes[$handle] = new GEndpoint($oSrc->component);
                $this->aStyles[$handle] = $oTemplate->getStaticStylesUrl();
                $this->addResource($handle, $oTemplate->output(), "template");
            } catch (Exception $e) {
                //@TODO log something here
            }
        }
    }

    public function registerRoutes()
    {
        foreach ($this->getARoutesData() as $slug => $v) {
            $this->add(
                $slug,
                \json_encode([
                    "template" => $this->getUrl($v, "template"),
                    "style" => "dupa",
                    "component" => $this->getUrl($v, "component")
                ]),
                $v["depends"]
            );
        }
    }

    /**
     * @return array|mixed|string
     */
    public function getARoutesData()
    {
        return $this->aRoutesData;
    }

    /**
     * @param array|mixed|string $aRoutesData
     */
    public function setARoutesData($aRoutesData): void
    {
        $this->aRoutesData = $aRoutesData;
    }

    private function getUrl(array $aData, string $context = "component")
    {
        global $geConfig;
        return str_replace(["//"], ["/"], $geConfig->get("routePrefix", "rest") . "/v" . $aData["version"] . "/" . $aData["endpoint"][$context]);
    }

    public function enqueueRoutes(genericCommand $oManifest = NULL)
    {
        if ($oManifest) {
            //@TODO Implement manifest handling
        } else {
            foreach ($this->registered as $handle => $item) {
                $this->enqueue($handle);
            }
        }
    }

    /**
     * @param string $handle
     * @param string $sContent
     */
    public function addResource(string $handle, string $sContent, string $sContext): void
    {
        $sName = "a" . ucwords($sContext) . "s";
        if (property_exists($this, $sName))
            $this->$sName[$handle] = $sContent;
    }

    public function getRouteData(string $sName): ?array
    {
        $aRoutesData = $this->getARoutesData();
        if (array_key_exists($sName, $aRoutesData)) {
            return $aRoutesData[$sName];
        }
        return null;
    }

    /**
     * @return string
     */
    public function getSFile(): string
    {
        return $this->sFile;
    }

    /**
     * @param string $sFile
     */
    public function setSFile(string $sFile): void
    {
        $this->sFile = $sFile;
    }

    /**
     * @return Logger
     */
    public function getOLogger(): Logger
    {
        return $this->oLogger;
    }

    /**
     * @param Logger $oLogger
     */
    public function setOLogger(Logger $oLogger): void
    {
        $this->oLogger = $oLogger;
    }

    /**
     * @return mixed
     */
    public function getARoutes()
    {
        return $this->aRoutes;
    }

    /**
     * @param mixed $aRoutes
     */
    public function setARoutes($aRoutes): void
    {
        $this->aRoutes = $aRoutes;
    }


}