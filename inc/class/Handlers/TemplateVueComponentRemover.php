<?php


namespace Gaad\GannerClient\Handlers;


use Gaad\GunnerClient\Interfaces\VueComponentCreatorInterface;
use Mustache_Engine;
use PHPUnit\Runner\Exception;
use Symfony\Component\Console\Input\ArgvInput;

class TemplateVueComponentRemover extends VueComponentGenericRemover
{
    /**
     * @param string $sVueComponentType
     * @param ArgvInput $input
     * @return bool
     */
    public function execute(string $sVueComponentType): bool
    {
        return parent::remove($sVueComponentType);
    }

    /**
     * @return array
     */
    public function getErrors(): array
    {
        return $this->errors;
    }

    /**
     * @return mixed
     */
    public function getVueComponent(): ?VueComponent
    {
        return $this->vueComponent;
    }

    /**
     * @return mixed
     */
    public function getBasePath()
    {
        return $this->basePath;
    }

    /**
     * @return string
     */
    public function getVueComponentType(): string
    {
        return $this->vueComponentType;
    }

    /**
     * @param string $vueComponentType
     */
    public function setVueComponentType(string $vueComponentType): void
    {
        $this->vueComponentType = $vueComponentType;
    }


}