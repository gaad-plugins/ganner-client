<?php


namespace Gaad\GannerClient\Handlers;


use Gaad\Gendpoints\Router\WpRestRoute;
use Gaad\Gendpoints\Router\WpRestRouter;
use Gaad\GunnerClient\Interfaces\VueComponentCreatorInterface;
use Mustache_Engine;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\ArgvInput;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class VueComponentGenericRemover
{

    protected $vueComponent;
    protected $basePath;
    protected $command;
    protected $input;
    protected $output;
    protected $vueComponentType = 'application';
    protected $errors = [];

    /**
     * ApplicationVueComponentGenericCreator constructor.
     * @param VueComponent $vueComponent
     * @param ArgvInput $input
     * @param OutputInterface $output
     */
    public function __construct(VueComponent $vueComponent, ArgvInput $input)
    {
        $this->input = $input;
        $this->vueComponent = $vueComponent;
        $this->basePath = $this->getVueComponent()->getPath();
    }

    /**
     * @param string $sVueComponentType
     * @param ArgvInput $input
     * @return bool
     * @throws \Exception
     */
    public function remove(string $sVueComponentType): bool
    {
        /*
         * Plan:
         * 1. remove from routes
         * 2. remove from templates
         * 3. remove from directory
         */
        $aStatus = [];
        $oRouter = new WpRestRouter([dirname(__GE2CLI_DIR__) . "/ganner-client/config"], "routes.yaml");
        $oVueComponents = new VueComponents([dirname(__GE2CLI_DIR__) . "/ganner-client/config"], "vue-components.yaml");

        //1
        $oRouter->delRouteByComponent($this->getVueComponent()->getContext() . "/" . $this->getVueComponent()->getName());
        $aStatus[] = (bool)@file_put_contents(dirname(__GE2CLI_DIR__) . "/ganner-client/config/routes.yaml", $oRouter->toYAML());

        //2
        $oVueComponents->delComponent($this->getVueComponent()->getName());
        $aStatus[] = (bool)@file_put_contents(dirname(__GE2CLI_DIR__) . "/ganner-client/config/vue-components.yaml", $oVueComponents->toYAML());

        //3
        try {
            $sPath = $this->getVueComponent()->getPath();
            if (!is_null($sPath)) {
                $this->recursivelyRmdir($sPath);
            }
        } catch (\Exception $e) {
            $aStatus[] = false;
        }

        return !in_array(false, $aStatus);
    }

    public function recursivelyRmdir($src): ?bool
    {
        if (!is_dir($src)) return false;
        $dir = opendir($src);
        if (!(bool)$dir) return false;

        while (false !== ($file = readdir($dir))) {
            if (($file != '.') && ($file != '..')) {
                $full = $src . '/' . $file;
                if (is_dir($full)) {
                    rrmdir($full);
                } else {
                    unlink($full);
                }
            }
        }
        closedir($dir);
        rmdir($src);
        return null;
    }

    public function missingComponentFileTemplateError(): void
    {
        $args = func_get_args();
        $this->addError("Template file `{$args[0]}` is not readable or doesn't exists.");
    }

    protected function addError(string $string)
    {
        $this->errors[] = $string;
    }

    /**
     * Writes Routes to file
     *
     * @return bool
     */
    public function updateRoutesFile(): bool
    {
        $oRouter = null;
        try {
            $oRouter = new WpRestRouter([dirname(__GE2CLI_DIR__) . "/ganner-client/config"], "routes.yaml");
            if ($oRouter) {
                $oVueComponent = $this->getVueComponent();
                $aRouteData = [
                    "method" => "GET",
                    "auth" => false,
                    "route" => $oVueComponent->getContext() . "/" . $oVueComponent->getName(),
                    "version" => $oVueComponent->getVersion(),
                    "defaults" => [
                        "noCache" => true,
                        "noModel" => true
                    ],
                    "dataTypes" => [
                        "default" => $oVueComponent::TYPE2DATA_TYPE[$oVueComponent->getType()]
                    ],
                ];
                $oRouter->addRoute(new WpRestRoute($aRouteData));
                return (bool)@file_put_contents(dirname(__GE2CLI_DIR__) . "/ganner-client/config/routes.yaml", $oRouter->toYAML());
            }
        } catch (\Exception $e) {
            //@TODO Implement error handling here
        }
        return false;
    }

    public function updateVueComponentsFile()
    {
        require_once dirname(__GE2CLI_DIR__) . "/../../wp-includes/class.wp-dependencies.php";
        require_once dirname(__GE2CLI_DIR__) . "/../../wp-includes/class-wp-dependency.php";

        $oVueComponents = null;
        try {
            $oVueComponents = new VueComponents([dirname(__GE2CLI_DIR__) . "/ganner-client/config"], "vue-components.yaml");
            if (is_object($oVueComponents)) {
                $oVueComponent = $this->getVueComponent();
                $oVueComponent->setFromOptions($this->input->getOptions());
                $oVueComponents->addComponent($oVueComponent);
                return (bool)@file_put_contents(dirname(__GE2CLI_DIR__) . "/ganner-client/config/vue-components.yaml", $oVueComponents->toYAML());
            }
        } catch (\Exception $e) {
            //@TODO Implement error handling here
        }
        return false;
    }

    public function getLastError()
    {
        return empty($this->getErrors()) ? "" : max($this->getErrors());
    }

    /**
     * @return array
     */
    public function getErrors(): array
    {
        return $this->errors;
    }
}