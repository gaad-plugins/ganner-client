<?php


namespace Gaad\GannerClient\Handlers;


use Gaad\Gendpoints\Router\WpRestRoute;
use Gaad\Gendpoints\Router\WpRestRouter;
use Gaad\GunnerClient\Interfaces\VueComponentCreatorInterface;
use Mustache_Engine;
use Symfony\Component\Console\Input\ArgvInput;
use Symfony\Component\Console\Input\InputInterface;

class VueComponentGenericCreator
{

    protected $vueComponent;
    protected $basePath;
    protected $input;
    protected $vueComponentType = 'application';
    protected $errors = [];

    /**
     * ApplicationVueComponentGenericCreator constructor.
     * @param VueComponent $vueComponent
     * @param InputInterface $input
     */
    public function __construct(VueComponent $vueComponent, ArgvInput $input)
    {
        $this->input = $input;
        $this->vueComponent = $vueComponent;
        $this->basePath = $this->getVueComponent()->getPath();
    }

    /**
     * @param string $sVueComponentType
     * @return bool
     */
    public function create(string $sVueComponentType)
    {
        if (!is_dir($this->getBasePath()))
            try {
                mkdir($this->getBasePath(), 0775, true);
            } catch (\Exception $e) {
                $this->addError("Creator error: `{$this->getBasePath()}` cannot be created.");
            }
        $this->setVueComponentType($sVueComponentType);
        $this->createFile('main');
        $this->createFile('model', ['modelExt' => 'php']);
        if (!$this->updateRoutesFile()) {
            $this->addError("Creator error: routes.yaml update issue.");
        }
        if (!$this->updateVueComponentsFile()) {
            $this->addError("Creator error: routes.yaml update issue.");
        }
        return 0 === count($this->getErrors());
    }

    protected function addError(string $string)
    {
        $this->errors[] = $string;
    }

    /**
     * @param string $sFilename
     * @param array|null $aData
     * @return bool|null
     */
    public function createFile(string $sFilename, array $aData = NULL): ?bool
    {
        $aData = !is_array($aData) ? (array)$aData : $aData;
        $oParser = new Mustache_Engine(array('entity_flags' => ENT_QUOTES));
        $oVueComponent = $this->getVueComponent();
        $sType = $oVueComponent->getType();
        $oVueComponentPath = $oVueComponent->getPath();
        $sComponentSubType = $this->input->getOption('subtype') ? "_" . $this->input->getOption('subtype') : "";
        $sFileExt = array_flip(VueComponent::EXT2TYPE)[$this->getVueComponentType()] ?? 'error';
        if (array_key_exists('modelExt', $aData)) {
            $sFileExt = $aData['modelExt'];
        }

        $sTemplatePath = explode("ganner-client", $oVueComponentPath)[0] . "ganner-client/inc/Templates/VueComponent-{$sType}-{$sFilename}{$sComponentSubType}.{$sFileExt}.tpl";
        $sBaseTemplatePath = explode("ganner-client", $oVueComponentPath)[0] . "ganner-client/inc/Templates/VueComponent-{$sType}-{$sFilename}.{$sFileExt}.tpl";
        if (!is_file($sTemplatePath)) {
            $sTemplatePath = $sBaseTemplatePath;
        }

        $sParsed = null;
        if (is_file($sTemplatePath)) {
            if ($sTemplate = @file_get_contents($sTemplatePath)) {
                $aTemplateData = [
                    'componentName' => strtolower($oVueComponent->getName())
                ];
                $sParsed = $oParser->render($sTemplate, $aTemplateData);
            } else {
                $this->missingComponentFileTemplateError($sTemplatePath);
            }
        } else {
            $this->missingComponentFileTemplateError($sTemplatePath);
        }

        if (count($this->getErrors()) > 0) return false;

        if ($sParsed) {
            $sTargetFilePath = $oVueComponentPath . "{$sFilename}." . $sFileExt;
            try {
                file_put_contents($sTargetFilePath, $sParsed);
                return true;
            } catch (\Exception $e) {
                $this->addError("File writing error. Target file: `{$sTargetFilePath}` ");
            }
        } else {
            $this->addError("File parsing error. Template file: `{$sTemplatePath}` ");;
            return false;
        }
        return null;
    }

    public function missingComponentFileTemplateError(): void
    {
        $args = func_get_args();
        $this->addError("Template file `{$args[0]}` is not readable or doesn't exists.");
    }

    /**
     * @return array
     */
    public function getErrors(): array
    {
        return $this->errors;
    }

    /**
     * Writes Routes to file
     *
     * @return bool
     */
    public function updateRoutesFile(): bool
    {
        $oRouter = null;
        try {
            $oRouter = new WpRestRouter([dirname(__GE2CLI_DIR__) . "/ganner-client/config"], "routes.yaml");
            if ($oRouter) {
                $oVueComponent = $this->getVueComponent();
                $aRouteData = [
                    "method" => "GET",
                    "auth" => false,
                    "route" => "/" . $oVueComponent::TYPE2DIR[$oVueComponent->getType()] . "/" . $oVueComponent->getContext() . "/" . $oVueComponent->getName(),
                    "version" => $oVueComponent->getVersion(),
                    "defaults" => [
                        "noCache" => true,
                        "noModel" => true
                    ],
                    "dataTypes" => [
                        "default" => $oVueComponent::TYPE2DATA_TYPE[$oVueComponent->getType()]
                    ],
                ];
                $oRouter->addRoute(new WpRestRoute($aRouteData), ucwords($this->getVueComponentType()));
                return (bool)@file_put_contents(dirname(__GE2CLI_DIR__) . "/ganner-client/config/routes.yaml", $oRouter->toYAML());
            }
        } catch (\Exception $e) {
            //@TODO Implement error handling here
        }
        return false;
    }

    public function updateVueComponentsFile()
    {
        require_once dirname(__GE2CLI_DIR__) . "/../../wp-includes/class.wp-dependencies.php";
        require_once dirname(__GE2CLI_DIR__) . "/../../wp-includes/class-wp-dependency.php";

        $oVueComponents = null;
        try {
            $oVueComponents = new VueComponents([dirname(__GE2CLI_DIR__) . "/ganner-client/config"], "vue-components.yaml");
            if (is_object($oVueComponents)) {
                $oVueComponent = $this->getVueComponent();
                $oVueComponent->setFromOptions($this->input->getOptions());
                $oVueComponents->addComponent($oVueComponent);
                return (bool)@file_put_contents(dirname(__GE2CLI_DIR__) . "/ganner-client/config/vue-components.yaml", $oVueComponents->toYAML());
            }
        } catch (\Exception $e) {
            //@TODO Implement error handling here
        }
        return false;
    }

    public function getLastError()
    {
        return empty($this->getErrors()) ? "" : max($this->getErrors());
    }
}