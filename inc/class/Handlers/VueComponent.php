<?php

namespace Gaad\GannerClient\Handlers;

use Gaad\GannerClient\Interfaces\genericCommand;
use Gaad\Gendpoints\Filesystem\DirSniffer;
use Symfony\Component\Console\Input\ArgvInput;

class VueComponent
{
    const EXT2TYPE = ['js' => 'application', 'php' => 'template'];
    const TYPE2DIR = ['application' => 'vue-component', 'template' => 'vue-template'];
    const TYPE2DATA_TYPE = ['application' => 'application/javascript', 'template' => 'template/javascript'];

    private $baseDir = "";
    private $name = "";
    private $version = 1;
    private $context = "";
    private $type = "not-exists";
    private $modelPath;
    private $main = "not-exists";
    private $depends = [];
    private $headers = [];
    private $params = [];
    private $errors = [];

    /**
     * VueComponent constructor.
     * @param string $baseDir
     * @param string $name
     * @param string $context
     * @param int $version
     * @param string $type
     */
    public function __construct(string $baseDir = '', string $name = 'new-component', string $context = 'test', int $version = 1, string $type = 'application')
    {
        $this->baseDir = $baseDir;
        $this->name = $name;
        $this->version = $version;
        $this->type = $type;
        $this->setContext($context);
        $this->type === "not-exists" ? $this->detectType() : false;
    }

    private function setContext(string $sContext): void
    {
        $sContextPart = self::TYPE2DIR[$this->type];
        $this->context = str_replace("//", "/", $sContextPart . "/" . $sContext);
    }

    /**
     * Detects component type by main file extension
     */
    private function detectType()
    {
        if (!is_dir($this->getPath())) return;
        foreach (new \DirectoryIterator($this->getPath()) as $item) {
            if ($item->isDir() || $item->isDot()) {
                continue;
            } elseif ("main." === str_replace($item->getExtension(), "", $item->getFilename()) && array_key_exists($item->getExtension(), $this::EXT2TYPE)) {
                $this->type = $this::EXT2TYPE[$item->getExtension()];
                continue;
            }

            if ("model.php" === $item->getFilename()) {
                $this->modelPath = $item->getPathname();
                continue;
            }
        }
    }

    /**
     * Return component path
     * @param bool $bRelative
     * @return string
     */
    public function getPath($bRelative = false)
    {
        return (!$bRelative ? $this->baseDir : "") . "/v{$this->version}/{$this->context}/{$this->name}/";
    }

    public function getLastError()
    {
        return empty($this->errors) ? "" : max($this->errors);
    }

    /**
     * Check if component exist and is valid
     */
    public function exists()
    {
        return "not-exists" !== $this->type && !empty($this->matchComponentDir());
    }

    /**
     * Match name, context and version to existing gendpoints directory tree.
     * Returns matched path or null.
     * @return string|null
     */
    public function matchComponentDir(): ?string
    {
        $name = $this->name;
        $context = $this->context;
        $version = $this->version;

        $aDirList = $this->getDomainDirList();
        if (empty($aDirList)) return false;
        foreach ($aDirList as $sDir) {
            $context_ = str_replace("/", "\/", $context);
            $sPattern = "/\/v{$version}\/{$context_}\/{$name}/";
            preg_match($sPattern, $sDir, $matches, PREG_OFFSET_CAPTURE, 0);
            if ($matches && !empty($matches) && 0 === strcmp($matches[0][0], $sDir)) {
                return $matches[0][0];
            }
        }
        return null;
    }

    /**
     * @return array
     */
    public function getDomainDirList()
    {
        $oDirSniffer = new DirSniffer($this->baseDir);
        $aList = [];
        foreach ($oDirSniffer->getArrayNumeric() as $sPath) {
            if (is_file($sPath)) continue;
            $aList[] = str_replace($this->baseDir, "", $sPath);
        }
        return $aList;
    }

    public function setFromOptionsArray(array $aOptions)
    {
        foreach ($aOptions as $sOptionName => $sOption) {
            if (property_exists($this, $sOptionName) && isset($sOption[0])) {
                $this->{$sOptionName} = $sOption;
            }
        }
    }

    public function setFromOptions(array $aOptions)
    {
        foreach ($aOptions as $sOptionName => $sOption) {
            if (property_exists($this, $sOptionName) && isset($sOption[0])) {
                $sOption = str_replace("=", "", $sOption);

                switch ($sOptionName) {
                    case 'params':
                    case 'headers' :
                        $a = [];
                        foreach (explode(",", $sOption) as $sPart) {
                            $sPart = explode(":", $sPart);
                            $a[$sPart[0]] = $sPart[1];
                        }
                        $this->{$sOptionName} = $a;
                        break;
                    default:
                        $this->{$sOptionName} = explode(",", $sOption);
                }
            }
        }
    }

    public function deleteFilesBulk(string $sVueComponentType, ArgvInput $input, array $aDependencies = [])
    {
        $aStatus = [];
        if (!empty($aDependencies)) {

            foreach ($aDependencies as $sDependencyName) {
                $dependentVueComponent = new VueComponent($this->baseDir, $sDependencyName, $this->getContext(), $this->version, $this->type);
                $aStatus[] = $dependentVueComponent->deleteFiles($sVueComponentType, $input);
            }
        }
        $aStatus[] = $this->deleteFiles($sVueComponentType, $input);
        return !in_array(false, $aStatus);
    }

    /**
     * @param string $sVueComponentType
     * @param ArgvInput $input
     * @return bool
     */
    public function deleteFiles(string $sVueComponentType, ArgvInput $input): bool
    {
        $sVueComponentRemoverClassName = __NAMESPACE__ . "\\" . ucwords($sVueComponentType) . "VueComponentRemover";
        if (class_exists($sVueComponentRemoverClassName)) {
            $oRemover = new $sVueComponentRemoverClassName($this, $input);
            $bStatus = $oRemover->execute($sVueComponentType);
            if (!$bStatus) {
                $aError = $oRemover->getErrors();
                foreach ($aError as $sError)
                    $this->addError("Remover error: " . $sError);
                return false;
            }
            return $bStatus;
        } else {
            $this->addError("Remover `{$sVueComponentRemoverClassName}` doesn't exists");
            return false;
        }
    }

    public function createFiles(string $sVueComponentType, ArgvInput $input)
    {
        $sSubType = $input->getOption('subtype') ? ucfirst($input->getOption('subtype')) : "";
        $sVueComponentBaseCreatorClassName = __NAMESPACE__ . "\\" . ucwords($sVueComponentType) . "VueComponentCreator";
        $sVueComponentCreatorClassName = __NAMESPACE__ . "\\" . ucwords($sVueComponentType) . $sSubType . "VueComponentCreator";
        if (!class_exists($sVueComponentCreatorClassName)) {
            $sVueComponentCreatorClassName = $sVueComponentBaseCreatorClassName;
        }

        if (class_exists($sVueComponentCreatorClassName)) {
            $oCreator = new $sVueComponentCreatorClassName($this, $input, strtolower($sSubType));
            $bStatus = $oCreator->create($sVueComponentType);
            if (!$bStatus) {
                $aError = $oCreator->getErrors();
                foreach ($aError as $sError)
                    $this->addError("Creator error: " . $sError);
                return false;
            }
            return $bStatus;
        } else {
            $this->addError("Creator `{$sVueComponentCreatorClassName}` doesn't exists");
            return false;
        }
    }

    private function addError(string $string)
    {
        $this->errors[] = $string;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return int
     */
    public function getVersion(): int
    {
        return $this->version;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @return mixed
     */
    public function getModelPath(): string
    {
        return $this->modelPath;
    }

    /**
     * @return array
     */
    public function getErrors(): array
    {
        return $this->errors;
    }

    /**
     * @return string
     */
    public function getContext(): string
    {
        $sContextPart = self::TYPE2DIR[$this->type];
        return implode("/", array_filter(explode("/", str_replace($sContextPart, "", $this->context))));
    }

    /**
     * @return array
     */
    public function getDepends(): array
    {
        return $this->depends;
    }

    /**
     * @return array
     */
    public function getHeaders(): array
    {
        return $this->headers;
    }

    /**
     * @return array
     */
    public function getParams(): array
    {
        return $this->params;
    }

}