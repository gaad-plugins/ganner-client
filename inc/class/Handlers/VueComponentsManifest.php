<?php


namespace Gaad\GannerClient\Handlers;


use Gaad\Gendpoints\Config\Config as Config;
use phpDocumentor\Reflection\Types\Array_;

class VueComponentsManifest
{
    private $routePath;
    private $routeSchema;
    private $routes;
    private $route;
    private $valid = false;
    private $queue;
    private $sFile = 'vue-routes.yaml';

    /**
     * VueComponentsManifest constructor.
     * @param string $route
     * @param \WP_Dependencies $queue
     */
    public function __construct(string $route, \WP_Dependencies $queue)
    {
        $this->queue = $queue;
        $this->routePath = $route;
        $this->routes = new Config($this->sFile, [__GANNERC_DIR__ . "/config"]);
        $this->route = $this->fetchRoute($this->routePath);
        $this->valid = !is_null($this->route);
    }

    private function fetchRoute(string $sRoute = NULL): ?array
    {
        $sRoute = !is_null($sRoute) ? $sRoute : $this->route;
        $aRoutes = $this->routes_();
        $aMatchedRoutes = [];
        foreach ($aRoutes as $sRouteIndex => $aRoute) {
            $re = $aRoute['regexp'];
            preg_match($re, $sRoute, $matches);
            if (!empty($matches)) {
                $this->routeSchema = $sRouteIndex;
                $aValues = array_slice($matches, 1);
                $aParams = [];
                $aParts = explode("/", $sRouteIndex);
                $next = false;

                foreach ($aParts as $partIndex => $partString) {
                    $bIndexMatched = $aRoute['indexes'][$partIndex];
                    if ($bIndexMatched) {
                        $paramValue = array_shift($aValues);
                        if (strrpos($paramValue, "/")) $next = true;
                        $aParams[str_replace(":", "", $partString)] = explode("/", $paramValue)[0];
                    }
                }

                if ($next) continue;

                return [
                    'path' => $sRoute,
                    'params' => $aParams,
                    'component' => $aRoute['component'],
                ];
            }
        }
        return null;
    }

    private function routes_(): array
    {
        $aRoutes = [];
        foreach ($this->routes->get() as $k => $route) {
            $aIndexes = [];
            $aParts = explode("/", $route['path']);

            foreach ($aParts as $k => $part) {
                if ($part === "") continue;
                if ($part[0] === ":") {
                    $aParts[$k] = "(.*)";
                    $aIndexes[$k] = substr($part, 1, strlen($part));
                }
            }
            $route["regexp"] = "/^" . implode("\\/", $aParts) . "$/";
            $route["indexes"] = $aIndexes;
            $route["paramsCount"] = array_filter($aIndexes);
            $aRoutes[$route["path"]] = $route;
        }
        return $aRoutes;
    }

    public function getComponets(): array
    {
        $aData = array_merge([$this->route['component']], $this->getGlobalComponentsNames());
        $aComponents = [];
        foreach ($aData as $k => $v) {
            if (array_key_exists($v, $this->queue->registered)) {
                $aComponents[$v] = $this->queue->registered[$v];
            }
        }

        return $aComponents;
    }

    private function getGlobalComponentsNames()
    {
        global $oGACVueComponents;
        $globalComponents = [];
        foreach ($oGACVueComponents->getAComponentsData() as $k => $v) {
            if (array_key_exists("global", $v) && (bool)$v['global']) {
                array_push($globalComponents, $k);
            }
        }
        return $globalComponents;
    }

    /**
     * @return bool
     */
    public function isValid(): bool
    {
        return $this->valid;
    }

    /**
     * @return mixed
     */
    public function getRouteSchema()
    {
        return $this->routeSchema;
    }

}