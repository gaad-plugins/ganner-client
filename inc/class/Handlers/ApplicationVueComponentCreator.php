<?php


namespace Gaad\GannerClient\Handlers;


use Gaad\GunnerClient\Interfaces\VueComponentCreatorInterface;
use Mustache_Engine;
use PHPUnit\Runner\Exception;

class ApplicationVueComponentCreator extends VueComponentGenericCreator
{


    /**
     * @param string $sFilename
     * @param array|null $aData
     * @return bool|null
     */
    public function createFile(string $sFilename, array $aData = NULL): ?bool
    {
        return parent::createFile($sFilename, $aData);
    }

    /**
     * @return array
     */
    public function getErrors(): array
    {
        return $this->errors;
    }

    /**
     * @return mixed
     */
    public function getVueComponent(): ?VueComponent
    {
        return $this->vueComponent;
    }

    /**
     * @return mixed
     */
    public function getBasePath()
    {
        return $this->basePath;
    }

    /**
     * @return string
     */
    public function getVueComponentType(): string
    {
        return $this->vueComponentType;
    }

    /**
     * @param string $vueComponentType
     */
    public function setVueComponentType(string $vueComponentType): void
    {
        $this->vueComponentType = $vueComponentType;
    }


}