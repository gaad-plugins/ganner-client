<?php


namespace Gaad\GannerClient\Handlers;


use Gaad\Command\genericCommand;
use Gaad\Gendpoints\Config\Config;
use Gaad\GunnerClient\Interfaces\VueComponentCreatorInterface;
use Mustache_Engine;
use PHPUnit\Runner\Exception;
use Symfony\Component\Console\Input\ArgvInput;

class ApplicationViewVueComponentCreator extends VueComponentGenericCreator
{
    public $subType = "";

    /**
     * ApplicationViewVueComponentCreator constructor.
     * @param VueComponent $vueComponent
     * @param ArgvInput $input
     * @param string $subType
     */
    public function __construct(VueComponent $vueComponent, ArgvInput $input, string $subType)
    {
        parent::__construct($vueComponent, $input);
        $this->subType = $subType;

    }

    public function create(string $sVueComponentType)
    {
        parent::create($sVueComponentType);
        if ("view" === $this->getSubType()) {
            $sComponentName = str_replace("-view-view", "-view", $this->input->getArgument('name') . "-view");
            $sCamelizedName = lcfirst(implode("", array_map("ucfirst", explode("-", $sComponentName))));

            $sRoutePath = str_replace("-view", "", $sComponentName);
            //$sRouteName = lcfirst(implode("", array_map("ucfirst", explode("-", str_replace("-view", "", $sComponentName)))));
            $sForceOverwrite = array_key_exists('force', $this->input->getOptions()) ? "--force" : "";
            $sDataEndpointCommand = "bash console.sh gendpoints2:create:data-endpoint {$sComponentName} ganner-client 1 ganner-client  --method POST {$sForceOverwrite}";
            ob_start();
            system($sDataEndpointCommand);
            $result = ob_get_clean();

            //updating general
            $configFilePath = dirname(__GE2CLI_DIR__) . "/ganner-client/config/general.yaml";
            $geConfig = new Config('general.yaml', [dirname($configFilePath)]);
            $aGEConfig = $geConfig->get();
            $aGEConfig['gannerc']['moduleDataEndpoint'][$sCamelizedName] = $this->getDataEndpointAddr($result);
            $geConfig->setAArray($aGEConfig);
            $saveResult = $geConfig->save($configFilePath, 'general');

            //updating vue-routes
            $vueRoutesFilePath = dirname(__GE2CLI_DIR__) . "/ganner-client/config/vue-routes.yaml";
            $geVueRoutes = new Config('vue-routes.yaml', [dirname($configFilePath)]);
            $aVueRoutes = $geVueRoutes->get();
            $aVueRoutes[$sRoutePath] = [
                'path' => '/' . $sRoutePath,
                'component' => str_replace("-view", "", $sComponentName)
            ];
            $geVueRoutes->setAArray($aVueRoutes);
            $saveResult = $geVueRoutes->save($vueRoutesFilePath, 'vue-routes');

            system("bash console.sh gannerc:generate:vue-routes");
        }


        return 0 === count($this->getErrors());
    }

    /**
     * @return string
     */
    public function getSubType(): string
    {
        return $this->subType;
    }

    public function getDataEndpointAddr(string $sCommandResult)
    {
        preg_match_all('/`\/(v.*\/)`/m', $sCommandResult, $matches, PREG_SET_ORDER, 0);
        if (!is_null($matches)) {
            return $matches[0][1];
        }
    }

    /**
     * @return array
     */
    public function getErrors(): array
    {
        return $this->errors;
    }

    /**
     * @param string $sFilename
     * @param array|null $aData
     * @return bool|null
     */
    public function createFile(string $sFilename, array $aData = NULL): ?bool
    {
        return parent::createFile($sFilename, $aData);
    }

    /**
     * @return mixed
     */
    public function getVueComponent(): ?VueComponent
    {
        return $this->vueComponent;
    }

    /**
     * @return mixed
     */
    public function getBasePath()
    {
        return $this->basePath;
    }

    /**
     * @return string
     */
    public function getVueComponentType(): string
    {
        return $this->vueComponentType;
    }

    /**
     * @param string $vueComponentType
     */
    public function setVueComponentType(string $vueComponentType): void
    {
        $this->vueComponentType = $vueComponentType;
    }


}