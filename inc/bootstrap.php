<?php

namespace Gaad\GunnerClient;

use Gaad\GannerClient\Handlers\VueComponents;
use Gaad\Gendpoints\Config\Config;

$GLOBALS['aGACConfigDirectories'] = [
    __GANNERC_DIR__ . '/config'
];

if (!defined('WP_CLI')) {
    \add_filter('ge-config-directories', '\Gaad\GunnerClient\Core\Filters\configDirectories');
    \add_filter('ge-endpoints-directories', '\Gaad\GunnerClient\Core\Filters\endpointsDirectories');

    if (is_admin()) {
        //Add wp-admin options page for planner Vue App
        \add_action('admin_menu', '\Gaad\GunnerClient\Core\Filters\OptionsPage');

        $geConfig = new Config('general.yaml', $GLOBALS['aGACConfigDirectories']);
        $sOptionsPageSlug = $geConfig->get('optionsPageSlug', 'gannerc');
        if (isset($_GET['page']) && $sOptionsPageSlug === $_GET['page']) {
            //External scripts loading
            \add_action('admin_enqueue_scripts', '\Gaad\GunnerClient\Core\Filters\buildAssetsQueue');
            \add_action('admin_enqueue_scripts', '\Gaad\GunnerClient\Core\Filters\buildStylesQueue');

            \add_action('admin_init', '\Gaad\GunnerClient\Core\Filters\gannercOptionsPageRedirect');
            //transport data from backend to frontend
            \add_action('in_admin_header', '\Gaad\GunnerClient\Core\Filters\backendDataTransport');
            //Vue templates
            \add_action('in_admin_header', '\Gaad\GunnerClient\Core\Filters\vueTemplatesLoading');
            //enable defer for vue app - waiting until components are loaded
            \add_action('script_loader_tag', '\Gaad\GunnerClient\Core\Filters\vueAppScriptDefer', 3, 10);
        }

    }

}

$GLOBALS['oGACVueComponents'] = new VueComponents($GLOBALS['aGACConfigDirectories']);