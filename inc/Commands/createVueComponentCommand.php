<?php

namespace Gaad\Command;

use Gaad\GannerClient\Handlers\VueComponent;
use Gaad\Ge2Cli\Interfaces\genericCommandInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class createVueComponentCommand extends genericCommand implements genericCommandInterface
{

    protected $sCommandName = 'gannerc:create:component';
    protected $aDefinition = [];
    protected $sDescription = 'Create Vue component and makes it available in the system.';
    protected $sHelp = 'Creates all necessary files and configurations for Vue component.';
    protected $aDirectories = [];
    protected $sBaseDir;
    protected $command;

    protected $aOptions = [];

    /**
     * @param $console
     */

    public function __construct($console)
    {
        $this->sBaseDir = dirname(__GE2CLI_DIR__) . "/ganner-client";
        parent::__construct($console);
    }


    /**
     * Method that do the command execution
     * @return \Closure
     */
    public function getCommandCode(): \Closure
    {
        return (function (InputInterface $input, OutputInterface $output) {
            $aStatus = [];
            $aStatus[] = $this->create($input, $output, 'application');
            $aStatus[] = $this->create($input, $output, 'template');

            if (!in_array(false, $aStatus)) {
                $output->writeln("<info>All component parts successfully created.</info>");
                return 0;
            } else return 1;

        });
    }

    public function create(InputInterface $input, OutputInterface $output, string $sType = 'application')
    {
        $sName = $input->getArgument('name');
        $sContext = $input->getArgument('context');
        $iVersion = $input->getArgument('version');
        $bForceOverwrite = array_key_exists('force', $input->getOptions());
        $sGEndpointsBaseDir = $this->sBaseDir . "/" . $this->config->get('gendpointsDir', 'dir');

        $oVueComponent = new VueComponent($sGEndpointsBaseDir, $sName, $sContext, $iVersion, $sType);
        if ($oVueComponent->exists() && !$bForceOverwrite) {
            $sErrorMsg = "Component `{$oVueComponent->getPath()}` already exists.";
            $output->writeln("<comment>{$sErrorMsg}</comment>");
            return false;
        } else {
            $bStatus = $oVueComponent->createFiles($sType, $input);
            if (!$bStatus) {
                $aError = $oVueComponent->getErrors();
                foreach ($aError as $sError)
                    $output->writeln("<comment>{$sError}</comment>");
                return false;
            } else {
                $reCreated = $bForceOverwrite ? "re" : "";
                $output->writeln("<info>Component {$oVueComponent->getType()} `{$oVueComponent->getPath(true)}` successfully {$reCreated}created.</info>");
                return true;
            }
        }
    }

    /**
     * @param string $target
     * @return array
     */
    public function getDirectories(string $target = 'all')
    {
        return parent::getDirectories($target);
    }

    public function addOptions(): void
    {
        $this->aOptions = [
            ['force', 'f', InputOption::VALUE_OPTIONAL, 'Force overwrite', ""],
            ['subtype', 'st', InputOption::VALUE_OPTIONAL, 'Set component subtype [ view ]', ""],
            ['depends', 'd', InputOption::VALUE_OPTIONAL, 'Set component dependency [other components names]', ""],
            ['headers', 'H', InputOption::VALUE_OPTIONAL, 'Set headers name:value', ""],
            ['params', 'p', InputOption::VALUE_OPTIONAL, 'Set params name:value', ""],
            ['method', 'm', InputOption::VALUE_OPTIONAL, 'Set request method', "GET"]
        ];
    }

    public function setDefinitions(): void
    {
        $this->setADefinition([
            new InputArgument('name', InputArgument::REQUIRED, 'Component name'),
            new InputArgument('context', InputArgument::REQUIRED, 'Component context'),
            new InputArgument('version', InputArgument::OPTIONAL, 'Component version', 1)
        ]);
    }

    /**
     * @return mixed
     */
    public function getConsole()
    {
        return $this->console;
    }

    /**
     * @param mixed $console
     */
    public function setConsole($console): void
    {
        $this->console = $console;
    }

    /**
     * @return string
     */
    public function getSCommandName(): string
    {
        return $this->sCommandName;
    }

    /**
     * @param string $sCommandName
     */
    public function setSCommandName(string $sCommandName): void
    {
        $this->sCommandName = $sCommandName;
    }

    /**
     * @return array
     */
    public function getADefinition(): array
    {
        return $this->aDefinition;
    }

    /**
     * @param array $aDefinition
     */
    public function setADefinition(array $aDefinition): void
    {
        $this->aDefinition = $aDefinition;
    }

    /**
     * @return string
     */
    public function getSDescription(): string
    {
        return $this->sDescription;
    }

    /**
     * @param string $sDescription
     */
    public function setSDescription(string $sDescription): void
    {
        $this->sDescription = $sDescription;
    }

    /**
     * @return string
     */
    public function getSHelp(): string
    {
        return $this->sHelp;
    }

    /**
     * @param string $sHelp
     */
    public function setSHelp(string $sHelp): void
    {
        $this->sHelp = $sHelp;
    }

    /**
     * @return mixed
     */
    public function getCommand()
    {
        return $this->command;
    }

}