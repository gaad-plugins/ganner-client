<?php

namespace Gaad\Command;

use Gaad\GannerClient\Handlers\VueAppRoutes;
use Gaad\GannerClient\Handlers\VueRoute;
use Gaad\GannerClient\Handlers\VueRoutes;
use Gaad\Ge2Cli\Interfaces\genericCommandInterface;
use Gaad\Gendpoints\Config\Config;
use Mustache_Engine;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Route\Console\Input\InputArgument;
use Symfony\Route\Console\Question\ChoiceQuestion;
use Symfony\Route\Console\Question\ConfirmationQuestion;

class genVueRoutesCommand extends genericCommand implements genericCommandInterface
{

    protected $sCommandName = 'gannerc:generate:vue-routes';
    protected $aDefinition = [];
    protected $sDescription = 'Generates routes json file for vue app.';
    protected $sHelp = '';
    protected $aDirectories = [];
    protected $sBaseDir;
    protected $command;
    protected $componentChildren = [];
    protected $VueRoute;

    protected $aOptions = [];
    private $input;

    /**
     * @param $console
     */

    public function __construct($console)
    {
        $this->sBaseDir = dirname(__GE2CLI_DIR__) . "/ganner-client";
        parent::__construct($console);
    }


    /**
     * Method that do the command execution
     * @return \Closure
     */
    public function getCommandCode(): \Closure
    {
        return (function (InputInterface $input, OutputInterface $output) {
            $aStatus = [];
            require_once dirname(__GE2CLI_DIR__) . "/../../wp-includes/class.wp-dependencies.php";
            require_once dirname(__GE2CLI_DIR__) . "/../../wp-includes/class-wp-dependency.php";
            $oVueAppRoutes = (new Config("vue-routes.yaml", [dirname(__GE2CLI_DIR__) . "/ganner-client/config"]))->get();
            $oParser = new Mustache_Engine(array('entity_flags' => ENT_QUOTES));
            $sTemplatePath = dirname(__GE2CLI_DIR__) . "/ganner-client/inc/Templates/VueRouter-routes.js.tpl";
            $sRouteTemplatePath = dirname(__GE2CLI_DIR__) . "/ganner-client/inc/Templates/VueRouter-route.tpl";
            $sOutputPath = dirname(__GE2CLI_DIR__) . "/ganner-client/assets/js/routes.js";
            $sParsed = null;
            if (is_file($sTemplatePath) && is_file($sRouteTemplatePath)) {
                $sTemplate = @file_get_contents($sTemplatePath);
                $sRouteTemplate = @file_get_contents($sRouteTemplatePath);
                if ($sTemplate && $sRouteTemplate) {
                    $aParsedRoutes = [];

                    foreach ($oVueAppRoutes as $sRouteName => $aRoute) {
                        $aRouteTemplateData = array_merge(["name" => $sRouteName], $aRoute);
                        $aParsedRoutes[] = $oParser->render($sRouteTemplate, $aRouteTemplateData);
                    }

                    $aTemplateData = [
                        'routes' => implode(",\n", $aParsedRoutes)
                    ];
                    $sParsed = $oParser->render($sTemplate, $aTemplateData);
                } else {
                    $this->missingRouteFileTemplateError($sTemplatePath);
                }
            } else {
                $this->missingRouteFileTemplateError($sTemplatePath);
            }

            if ($sParsed) {
                file_put_contents($sOutputPath, str_replace(["&quot;"], ["\""], $sParsed));
            }
            return 0;
        });
    }

    public function missingRouteFileTemplateError(): void
    {
        $args = func_get_args();
        $this->addError("Template file `{$args[0]}` is not readable or doesn't exists.");
    }

    public function addOptions(): void
    {
    }

    public function setDefinitions(): void
    {
        $this->setADefinition([
        ]);
    }

    /**
     * @return mixed
     */
    public function getConsole()
    {
        return $this->console;
    }

    /**
     * @param mixed $console
     */
    public function setConsole($console): void
    {
        $this->console = $console;
    }

    /**
     * @return string
     */
    public function getSCommandName(): string
    {
        return $this->sCommandName;
    }

    /**
     * @param string $sCommandName
     */
    public function setSCommandName(string $sCommandName): void
    {
        $this->sCommandName = $sCommandName;
    }

    /**
     * @return array
     */
    public function getADefinition(): array
    {
        return $this->aDefinition;
    }

    /**
     * @param array $aDefinition
     */
    public function setADefinition(array $aDefinition): void
    {
        $this->aDefinition = $aDefinition;
    }

    /**
     * @return string
     */
    public function getSDescription(): string
    {
        return $this->sDescription;
    }

    /**
     * @param string $sDescription
     */
    public function setSDescription(string $sDescription): void
    {
        $this->sDescription = $sDescription;
    }

    /**
     * @return string
     */
    public function getSHelp(): string
    {
        return $this->sHelp;
    }

    /**
     * @param string $sHelp
     */
    public function setSHelp(string $sHelp): void
    {
        $this->sHelp = $sHelp;
    }

    /**
     * @return mixed
     */
    public function getCommand()
    {
        return $this->command;
    }

}