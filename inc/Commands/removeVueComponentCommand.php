<?php

namespace Gaad\Command;

use Gaad\GannerClient\Handlers\VueComponent;
use Gaad\GannerClient\Handlers\VueComponents;
use Gaad\Ge2Cli\Interfaces\genericCommandInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ChoiceQuestion;
use Symfony\Component\Console\Question\ConfirmationQuestion;

class removeVueComponentCommand extends genericCommand implements genericCommandInterface
{
    const ANSWER_YES = 'Yes';
    const ANSWER_NO = 'No';
    const ANSWER_REMOVE_MASTER = 'Remove only master component and let me handle frontend errors myself';

    protected $sCommandName = 'gannerc:remove:component';
    protected $aDefinition = [];
    protected $sDescription = 'Removes Vue component from the system.';
    protected $sHelp = 'Completely remove Vue component and all dependent ones.';
    protected $aDirectories = [];
    protected $sBaseDir;
    protected $command;
    protected $componentChildren = [];
    protected $VueComponent;

    protected $aOptions = [];
    private $input;

    /**
     * @param $console
     */

    public function __construct($console)
    {
        $this->sBaseDir = dirname(__GE2CLI_DIR__) . "/ganner-client";
        parent::__construct($console);
    }


    /**
     * Method that do the command execution
     * @return \Closure
     */
    public function getCommandCode(): \Closure
    {
        return (function (InputInterface $input, OutputInterface $output) {
            $aStatus = [];
            //are you sure to remove  question handling
            $this->input = $input;
            $sName = $input->getArgument('name');
            $this->getComponentChildren($sName);
            $aDependencies = [];
            if (!empty($this->componentChildren)) {
                $output->writeln([
                    '',
                    '============================',
                    'Warning! Dependencies found.',
                    '============================',
                    '',
                ]);
                $output->writeln(["Listed components needs to be removed too."]);

                foreach ($this->componentChildren as $sSlug) {
                    $aDependencies[] = $sSlug;
                }
                $sComment = implode(", ", $aDependencies);
                $output->writeln(["<comment>{$sComment}</comment>", ""]);
            }

            $question = new ChoiceQuestion('Are You sure that you want to remove this component and all dependent ones? ', [self::ANSWER_NO, self::ANSWER_YES, self::ANSWER_REMOVE_MASTER]);
            $helper = $this->command->getHelper('question');
            $answer = $helper->ask($input, $output, $question);
            if (self::ANSWER_NO === $answer) {
                return false;
            }
            if (self::ANSWER_YES === $answer) {
                $removeMode = 'all';
            }
            if (self::ANSWER_REMOVE_MASTER === $answer) {
                $removeMode = 'master';
            }

            $aStatus[] = $this->remove($input, $output, 'application', $removeMode, $aDependencies);
            $aStatus[] = $this->remove($input, $output, 'template', $removeMode, $aDependencies);

            if (!in_array(false, $aStatus)) {
                $output->writeln("<info>All component parts successfully removed.</info>");
                return 0;
            } else return 1;

        });
    }

    public function getComponentChildren($slug)
    {
        require_once dirname(__GE2CLI_DIR__) . "/../../wp-includes/class.wp-dependencies.php";
        require_once dirname(__GE2CLI_DIR__) . "/../../wp-includes/class-wp-dependency.php";
        $sName = $this->input->getArgument('name');
        $oVueComponents = new VueComponents([dirname(__GE2CLI_DIR__) . "/ganner-client/config"], "vue-components.yaml");
        foreach ($oVueComponents->getAComponentsData() as $sSlug => $aComponent) {
            if (in_array($slug, is_array($aComponent['depends']) ? $aComponent['depends'] : [])) {
                if (in_array($sSlug, $this->componentChildren)) continue;
                if ($sSlug === $sName) continue;
                $this->componentChildren[$sSlug] = $sSlug;
                $this->getComponentChildren($sSlug);
            }
        }
    }

    public function remove(InputInterface $input, OutputInterface $output, string $sType = 'application', string $sRemoveMode, array $aDependencies = NULL)
    {
        $sName = $input->getArgument('name');
        $sContext = $input->getArgument('context');
        $iVersion = $input->getArgument('version');
        $sGEndpointsBaseDir = $this->sBaseDir . "/" . $this->config->get('gendpointsDir', 'dir');

        $this->VueComponent = new VueComponent($sGEndpointsBaseDir, $sName, $sContext, $iVersion, $sType);
        if (!$this->VueComponent->exists()) {
            $sErrorMsg = "Component `{$this->VueComponent->getPath()}` doesn't exists.";
            $output->writeln("<comment>{$sErrorMsg}</comment>");
            return false;
        } else {
            $sDeleteMethod = $sRemoveMode === "master" ? "deleteFiles" : "deleteFilesBulk";
            $aStatus[] = $this->VueComponent->$sDeleteMethod('application', $input, (array)$aDependencies);
            $aStatus[] = $this->VueComponent->$sDeleteMethod('template', $input, (array)$aDependencies);
            return !in_array(false, $aStatus);
        }
    }

    /**
     * @param string $target
     * @return array
     */
    public function getDirectories(string $target = 'all')
    {
        return parent::getDirectories($target);
    }

    public function addOptions(): void
    {
    }

    public function setDefinitions(): void
    {
        $this->setADefinition([
            new InputArgument('name', InputArgument::REQUIRED, 'Component name'),
            new InputArgument('context', InputArgument::REQUIRED, 'Component context'),
            new InputArgument('version', InputArgument::OPTIONAL, 'Component version', 1)
        ]);
    }

    /**
     * @return mixed
     */
    public function getConsole()
    {
        return $this->console;
    }

    /**
     * @param mixed $console
     */
    public function setConsole($console): void
    {
        $this->console = $console;
    }

    /**
     * @return string
     */
    public function getSCommandName(): string
    {
        return $this->sCommandName;
    }

    /**
     * @param string $sCommandName
     */
    public function setSCommandName(string $sCommandName): void
    {
        $this->sCommandName = $sCommandName;
    }

    /**
     * @return array
     */
    public function getADefinition(): array
    {
        return $this->aDefinition;
    }

    /**
     * @param array $aDefinition
     */
    public function setADefinition(array $aDefinition): void
    {
        $this->aDefinition = $aDefinition;
    }

    /**
     * @return string
     */
    public function getSDescription(): string
    {
        return $this->sDescription;
    }

    /**
     * @param string $sDescription
     */
    public function setSDescription(string $sDescription): void
    {
        $this->sDescription = $sDescription;
    }

    /**
     * @return string
     */
    public function getSHelp(): string
    {
        return $this->sHelp;
    }

    /**
     * @param string $sHelp
     */
    public function setSHelp(string $sHelp): void
    {
        $this->sHelp = $sHelp;
    }

    /**
     * @return mixed
     */
    public function getCommand()
    {
        return $this->command;
    }

}