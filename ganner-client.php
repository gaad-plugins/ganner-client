<?php
/*
 Plugin Name: Ganner Client
 Plugin URI:
 Description: GAAD Planner API Frontend Client.
 Version: 0.1
 Author: Bartek GAAD Sosnowski
 Author URI:
 Text Domain: gannerc
 */

define('__GANNERC_DIR__', __DIR__);
define('__GANNERC_URI__', explode("wp-content", get_stylesheet_directory_uri())[0] . "wp-content/plugins/" . basename(__GANNERC_DIR__));


file_exists(__DIR__ . "/vendor/autoload.php") ? require_once __DIR__ . "/vendor/autoload.php" : null;
$wp_dotenv = Dotenv\Dotenv::createImmutable(__DIR__ . '/../../../'); // wp root
$wp_dotenv->load();
define('ENV', getenv('PAGE_ENV'));
include_once __GANNERC_DIR__ . "/inc/bootstrap.php";