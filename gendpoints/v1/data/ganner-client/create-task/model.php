<?php
//create-task model

use Gaad\Gendpoints\GEndpoint;
use Gaad\Gendpoints\Handlers\Rest\GenericOutputDataHandler;

$aHeaders = $this->getAllHeaders();
$iBucketID = "backlog" === $aHeaders["Bucket"] ? NULL : (int)$aHeaders["Bucket"];
$sName = $aHeaders["Name"];
$sType = isset($aHeaders["Type"]) ? $aHeaders["Type"] : 'task';
$iOwnerID = isset($aHeaders["Owner"]) ? (int)$aHeaders["Owner"] : 1;
$sGeDataOrigin = $aHeaders['Ge-Data-Origin'];
$iCacheAlive = isset($aHeaders['Cache-Alive']) ? (int)$aHeaders['Cache-Alive'] : 600;
$actionAuthorized = $this->authorizeAction(pathinfo(__DIR__)['basename']);
$aOutput = [];

if ($actionAuthorized) {
    $oGECreateTask = new GEndpoint("/wp-json/groute/v1/ganner/task/create", [
        "bDisableAuth" => true,
        "aHeaders" => [
            "Project" => 1,
            "Bucket" => $iBucketID,
            "Owner" => $iOwnerID,
            "Type" => $sType,
            "Name" => $sName
        ]
    ]);
    $aResult = $oGECreateTask->outputArray();
    $iTaskID = $aResult['data']["created_id"];

    $oGETaskList = new GEndpoint("/wp-json/groute/v1/ganner/task/list", [
        "bDisableAuth" => true,
        "aHeaders" => [
            "Project" => 1,
            "Criteria-and-1" => "eq('id',{$iTaskID})"
        ]
    ]);
    $aResult = $oGETaskList->outputArray();
    $aOutput = $aResult['data']['list'][0];
} else {
    $oError = new GenericOutputDataHandler("error", ["message" => "Operation not permitted", "code" => 403]);
    $this->setSStatus('error');
    $this->setICode(GEndpoint::ACTION_FORBIDDEN_CODE);
    $this->oModel->addData(GEndpoint::ACTION_FORBIDDEN_CODE, 'code');
    $aOutput = $oError->asArray();
}

$this->oModel->addData($sGeDataOrigin, "Ge-Data-Origin");
$this->oModel->addData($iCacheAlive, 'Cache-Alive');
$this->oModel->addData($aOutput, "Module-Data");