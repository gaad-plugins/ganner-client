<?php
//create-bucket model

use Gaad\Gendpoints\GEndpoint;
use Gaad\Gendpoints\Handlers\Rest\GenericOutputDataHandler;

$aHeaders = $this->getAllHeaders();
$sGeDataOrigin = $aHeaders['Ge-Data-Origin'];
$iCacheAlive = isset($aHeaders['Cache-Alive']) ? (int)$aHeaders['Cache-Alive'] : 600;
$sName = $aHeaders["Name"];
$iOwnerID = isset($aHeaders["Owner"]) ? (int)$aHeaders["Owner"] : 1;
$actionAuthorized = $this->authorizeAction(pathinfo(__DIR__)['basename']);
$aOutput = [];

if ($actionAuthorized) {
    $oGECreateBucket = new GEndpoint("/wp-json/groute/v1/ganner/bucket/create", [
        "bDisableAuth" => true,
        "aHeaders" => [
            "Project" => 1,
            "Owner" => $iOwnerID,
            "Name" => $sName
        ]
    ]);
    $aResult = $oGECreateBucket->outputArray();
    $iBucketID = $aResult['data']["created_id"];
    $aOutput = [];
    if ($iBucketID) {
        $aOutput['created_id'] = $iBucketID;
    }
} else {
    $oError = new GenericOutputDataHandler("error", ["message" => "Operation not permitted (bucket creation)", "code" => 403]);
    $this->setSStatus('error');
    $this->setICode(GEndpoint::ACTION_FORBIDDEN_CODE);
    $this->oModel->addData(GEndpoint::ACTION_FORBIDDEN_CODE, 'code');
    $aOutput = $oError->asArray();
}


$this->oModel->addData($sGeDataOrigin, "Ge-Data-Origin");
$this->oModel->addData($iCacheAlive, 'Cache-Alive');
$this->oModel->addData($aOutput, "Module-Data");