<?php
//starter-data model
use Gaad\Gendpoints\GEndpoint;

$aHeaders = $this->getAllHeaders();
$sGeDataOrigin = $aHeaders['Ge-Data-Origin'];
$iCacheAlive = isset($aHeaders['Cache-Alive']) ? (int)$aHeaders['Cache-Alive'] : 600;

$oGEBuckets = new GEndpoint("/wp-json/groute/v1/ganner/bucket/list", [
    "bDisableAuth" => true,
    "aHeaders" => [
        "Project" => 1,
        "Perpage" => 10000
    ]
]);
$oGETasks = new GEndpoint("/wp-json/groute/v1/ganner/task/list", [
    "bDisableAuth" => true,
    "aHeaders" => [
        "Project" => 1,
        "Perpage" => 10000,
        "Criteria-or-1" => "eq('status','active')",
        "Criteria-or-2" => "eq('status','blocked')"
    ]
]);
$aBuckets = $oGEBuckets->outputArray();
$aBuckets = is_array($aBuckets['data']['list']) ? $aBuckets['data']['list'] : [];
$aTasks = $oGETasks->outputArray();
$aTasks = is_array($aTasks['data']['list']) ? $aTasks['data']['list'] : [];
$aOutput = [
    'aBuckets' => $aBuckets,
    'aTasks' => $aTasks,
];
$sOutput = json_encode($aOutput);
$sOutput = urldecode($sOutput);
$aOutput = json_decode($sOutput, true);


$this->oModel->addData($sGeDataOrigin, "Ge-Data-Origin");
$this->oModel->addData($iCacheAlive, 'Cache-Alive');
$this->oModel->addData($aOutput, "Module-Data");