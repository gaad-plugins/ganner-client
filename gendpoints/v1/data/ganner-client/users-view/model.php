<?php
//users-view model

use Gaad\Gendpoints\Config\Config;
use Gaad\Gendpoints\GEndpoint;
use Gaad\Gendpoints\Handlers\Rest\GenericOutputDataHandler;

$aHeaders = $this->getAllHeaders();
$sGeDataOrigin = $aHeaders['Ge-Data-Origin'];
$iCacheAlive = isset($aHeaders['Cache-Alive']) ? (int)$aHeaders['Cache-Alive'] : 600;
$actionAuthorized = $this->authorizeAction(pathinfo(__DIR__)['basename']);
$aOutput = [
];

function getUsersWithRole($roles)
{
    global $wpdb;
    if (!is_array($roles)) {
        $explode = explode(",", $roles);
        $roles = array_walk($explode, 'trim');
        $roles = $explode;
    }
    $sql = '
        SELECT  user_id AS id, user_login AS login, user_email AS email, user_registered AS registered, wp_usermeta.meta_value as caps
        FROM        ' . $wpdb->users . ' INNER JOIN ' . $wpdb->usermeta . '
        ON          ' . $wpdb->users . '.ID             =       ' . $wpdb->usermeta . '.user_id
        WHERE       ' . $wpdb->usermeta . '.meta_key        =       \'' . $wpdb->prefix . 'capabilities\'
        AND     (
    ';
    $i = 1;
    foreach ($roles as $role) {
        $sql .= ' ' . $wpdb->usermeta . '.meta_value    LIKE    \'%"' . $role . '"%\' ';
        if ($i < count($roles)) $sql .= ' OR ';
        $i++;
    }
    $sql .= ' ) ';
    $sql .= ' ORDER BY display_name ';
    $results = $wpdb->get_results($sql, ARRAY_A);
    if (!empty($results))
        foreach ($results as $index => $user) {
            $results[$index]['caps'] = unserialize($user['caps']);
        }
    return $results;
}

if ($actionAuthorized) {
    global $geConfig;
    $aRolesYaml = $geConfig->get('roles-yaml', 'wp-roles');
    $oGannerWPRoles = new Config(basename($aRolesYaml), [__GANNER_DIR__ . "/config"]);
    $aOutput = [
        'aUsers' => getUsersWithRole(implode(",", array_keys($oGannerWPRoles->get())))
    ];
} else {
    $oError = new GenericOutputDataHandler("error", ["message" => "Operation not permitted (Users list)", "code" => 403]);
    $this->setSStatus('error');
    $this->setICode(GEndpoint::ACTION_FORBIDDEN_CODE);
    $this->oModel->addData(GEndpoint::ACTION_FORBIDDEN_CODE, 'code');
    $aOutput = $oError->asArray();
}
$this->oModel->addData($sGeDataOrigin, "Ge-Data-Origin");
$this->oModel->addData($iCacheAlive, 'Cache-Alive');
$this->oModel->addData($aOutput, "Module-Data");