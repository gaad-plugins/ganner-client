<?php

use Gaad\GannerClient\Handlers\VueComponentsManifest;

$aHeaders = $this->getAllHeaders();
$sRoute = $aHeaders['Route'];
$sComponent = $aHeaders['Component'];
$sRouteSchema = $aHeaders['Routeschema'];
$aIgnore = explode(",", $aHeaders['Ignore']);
global $geConfig, $oGACVueComponents;

$sGeDataOrigin = $aHeaders['Ge-Data-Origin'];
$iCacheAlive = isset($aHeaders['Cache-Alive']) ? (int)$aHeaders['Cache-Alive'] : 600;
$this->oModel->addData($sGeDataOrigin, "Ge-Data-Origin");
$this->oModel->addData($iCacheAlive, 'Cache-Alive');

$oGACVueComponents->registerComponents();
$oGACVueComponents->fetchComponents($sRoute, $aIgnore);
$componentsNeeded = $oGACVueComponents->getAComponents();
$componentsNeededData = [];
if (!empty($componentsNeeded))
    foreach ($componentsNeeded as $handle => $oComponent) {
        $componentsNeededData[$handle] = [
            "template" => $oGACVueComponents->getATemplates()[$handle],
            'style' => $oGACVueComponents->getAStyles()[$handle],
            "appCode" => $oComponent->output(),
            "appMain" => $oComponent->getFullUri() . '/main.js'
        ];
    }


$this->oModel->addData($geConfig->get("vueTemplateHandlePrefix", "gannerc"), "VueTemplateHandlePrefix");
$this->oModel->addData($sComponent, "component");
$this->oModel->addData($sRouteSchema, "routeSchema");
$this->oModel->addData($componentsNeededData, "componentsData");
$this->oModel->addData($sRoute, "path");