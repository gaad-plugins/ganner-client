window['create-taskComponentObject'] = Vue.component('create-task', {
    template: '#gannerc-template-create-task',
    data: function () {
        var storeModuleName = 'create-task-view'.camelize();
        return {
            dataLoading: this.$root.dataLoading,
            storeModuleName: storeModuleName,
            storageType: 'sessionStorage'
        }
    },

    beforeCreate: function () {
        var cacheDisabled = geConfig.gannerc.sessionStorageCacheDisabled;
        if (!cacheDisabled) {
            var LocStorStats_ = new LocStorStats(geConfig.gannerc.sessionStorageCollectionName || 'gc', 'sessionStorage');
            LocStorStats_.clearCache();
        }
    },

    created: function () {
        this.$root.bus.$on('dataLoadingDone', this.dataLoadingDone);
        this.$root.bus.$on('cache-cleared', this.cacheClearedCallback);
    },

    watch: {
        "$root.dataLoading": function () {
            this.dataLoading = this.$root.dataLoading;
        }
    },

    mounted: function () {
        this.taskID = this.$root.getRouteParam('id');

        if (this.$root.dataLoadedIndex.indexOf(this.storeModuleName) === -1) {
            this.$root.appDataLoadingOn();
            this.dataLoading = true;
            this.storeModuleName ?
                this.$root.enqueueApiData(new apiDataRequest(this.storeModuleName, {
                    type: 'post',
                    loadingMsg: "Loading notes",
                    alive: this.$root.getModuleCacheAlive('create-task-view'.camelize()),
                    endpoint: this.$root.getModuleEndpoint('create-task-view'.camelize()),
                    headers: {
                        Id: this.taskID
                    }
                }, this.$root)) : false;
        } else {
            this.$root.appDataLoadingOff();
            this.dataLoading = false;
        }
    },

    methods: {
        cacheClearedCallback: function (payload) {
            this.$root.cacheClearedCallback(payload, geConfig.gannerc[this.storageType + 'CollectionName'] + '_' + this.storeModuleName);
        },

        dataLoadingDone: function () {
            this.dataLoading = false;
        }
    }
});