var taskViewComponentObject = Vue.component('task-view', {
    template: '#gannerc-template-task-view',

    data: function () {
        return {
            dataLoading: this.$root.dataLoading,
            taskID: null,
            storeModuleName: null
        }
    },

    /*beforeCreate: function () {
        var cacheDisabled = geConfig.gannerc.sessionStorageCacheDisabled;
        if (!cacheDisabled) {
            var LocStorStats_ = new LocStorStats(geConfig.gannerc.localStorageCollectionName || 'gc');
            LocStorStats_.clearCache();
        }
    },*/

    created: function () {
        this.$root.bus.$on('dataLoadingDone', this.dataLoadingDone);
        this.$root.bus.$on('cache-cleared', this.cacheClearedCallback);
    },

    watch: {
        "$root.dataLoading": function () {
            this.dataLoading = this.$root.dataLoading;
        }
    },

    mounted: function () {
        this.taskID = this.$root.getRouteParam('id');
        this.storeModuleName = 'taskModel_' + this.taskID;

        if (this.$root.dataLoadedIndex.indexOf(this.storeModuleName) === -1) {
            this.$root.appDataLoadingOn();
            this.dataLoading = true;
            this.$root.enqueueApiData(new apiDataRequest(this.storeModuleName, {
                type: 'post',
                loadingMsg: "Loading task content",
                alive: this.$root.getModuleCacheAlive('taskModel'),
                endpoint: this.$root.getModuleEndpoint('taskModel'),
                headers: {
                    Id: this.taskID
                }
            }, this.$root));
        } else {
            this.$root.appDataLoadingOff();
            this.dataLoading = false;
        }
    },

    methods: {
        cacheClearedCallback: function (payload) {
            this.$root.cacheClearedCallback(payload, geConfig.gannerc.localStorageCollectionName + '_' + this.storeModuleName);
        },

        dataLoadingDone: function () {
            this.dataLoading = false;
        }
    }
});