var indexViewComponentObject = Vue.component('index-view', {
    template: '#gannerc-template-index-view',
    data: function () {
        var storeModuleName = 'theList';

        return {
            dataLoading: this.$root.dataLoading,
            storeModuleName: storeModuleName,
            storageType: 'sessionStorage',
            collectionName: geConfig.gannerc.sessionStorageCollectionName || 'gc',
            preloader: null,
            loadDataArguments: {
                loadingMsg: "Loading tasks list",
                type: 'post',
                alive: this.$root.getModuleCacheAlive(storeModuleName),
                endpoint: this.$root.getModuleEndpoint(storeModuleName)
            }
        }
    },
    watch: {
        "$root.dataLoading": function () {
            this.dataLoading = this.$root.dataLoading;
        },

        "$root.manifestLoading": function () {
            this.setupPreloader();
        }
    },

    beforeCreate: function () {
        var cacheDisabled = geConfig.gannerc.sessionStorageCacheDisabled;
        if (!cacheDisabled) {
            var LocStorStats_ = new LocStorStats(geConfig.gannerc.sessionStorageCollectionName || 'gc', 'sessionStorage');
            LocStorStats_.clearCache();
        }
    },

    beforeMount: function () {
        /*
        Check if proper data is loaded from backend. No data could be the case when app is loaded from differen root than index-view /
        If data is missing, trigger reload
        */
        var theModule = this.$store.state[this.storeModuleName + 'Module'];
        if (undefined === theModule) {
            if (this.$root.dataLoadedIndex.indexOf(this.storeModuleName) !== -1) {
                this.$root.removeDataLoadedIndex(this.storeModuleName);
            }
            this.loadData();
        }

        this.setupPreloader();
    },

    created: function () {
        this.$root.bus.$on('dataLoadingDone', this.dataLoadingDone);
        this.$root.bus.$on('cache-cleared', this.cacheClearedCallback);
        this.$root.bus.$on('theList-updated', this.dataUpdate);
    },

    mounted: function () {
        this.loadData();
    },

    methods: {

        dataUpdate: function (payload) {

        },

        setupPreloader: function () {
            if (!this.preloader) {
                this.preloader = new window.LocStorPreload(this.collectionName, 'sessionStorage', {
                    loadDataArguments: this.loadDataArguments,
                    key: this.storeModuleName,
                    success: this.dataPreloadingDone,
                    enabled: this.checkComponent,
                    interval: this.$root.getModulePreloadInterval(this.storeModuleName)
                });
            }
        },

        loadData: function () {
            if (this.$root.dataLoadedIndex.indexOf(this.storeModuleName) === -1) {
                this.$root.enqueueApiData(new apiDataRequest(this.storeModuleName, this.loadDataArguments));
            } else {
                this.dataLoadingDone();
            }

        },

        cacheClearedCallback: function (payload) {
            this.$root.cacheClearedCallback(payload, geConfig.gannerc.localStorageCollectionName + '_' + this.storeModuleName);
        },

        checkComponent: function () {
            return jQuery("." + this._vnode.data.staticClass).is(":visible");
        },

        dataLoadingDone: function () {
            this.$root.appDataLoadingOff();
            this.dataLoading = false;
        }
    }
});