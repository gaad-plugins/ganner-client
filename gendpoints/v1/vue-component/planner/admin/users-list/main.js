window['users-listComponentObject'] = Vue.component('users-list', {
    template: '#gannerc-template-users-list',
    data: function () {
        var usersListModule = this.$store.state.usersListModule;
        var aUsers = undefined !== usersListModule ? this.$store.state.usersListModule.aUsers : [];
        return {
            users: aUsers
        }
    },

    methods: {}
});