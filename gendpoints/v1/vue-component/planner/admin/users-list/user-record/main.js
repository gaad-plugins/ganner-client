window['user-recordComponentObject'] = Vue.component('user-record', {
    template: '#gannerc-template-user-record',
    props: ['data'],
    data: function () {
        return {
            data_: undefined !== this.data ? this.data : []
        }
    },

    methods: {}
});