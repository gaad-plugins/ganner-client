window['restricted-accessComponentObject'] = Vue.component('restricted-access', {
    template: '#gannerc-template-restricted-access',
    props: ['msg', 'needCaps', 'ifFailed', 'fallbackComponent'],

    data: function () {
        return {
            authorized: false,
            component_: undefined === this.getComponent() ? "not-found-404" : this.getComponent(),
        }
    },

    computed: {
        component: {
            set: function (value) {
                this.component_ = value;
            },
            get: function () {
                debugger
                /*var oRegExp = new RegExp('[&]' + this.srcparam + '=([^&.]*)', 'i');
                var currentRoute = oRegExp.exec(window.location.href)[1];
                var currentRouteComponentVar = this.$root.getRoute(decodeURIComponent(currentRoute)).component;
                return typeof window[currentRouteComponentVar + 'ComponentObject'] === "function" ? currentRouteComponentVar : 'app404RouteComponentVar';*/
            }
        },

        caps: function () {
            return this.needCaps.split(',');
        }
    },

    created: function () {
        this.authorized = this.$root.checkUserCaps(this.caps);
    },

    methods: {

        getComponent: function (e) {
            return this.component;
        },
    }
});