window['loadingComponentObject'] = Vue.component('loading', {
    props: ["loadingMsg"],
    template: '#gannerc-template-loading',
    data: function () {
        return {
            msg: this.$root.loadingMsg,
            loadingMsg_: undefined === this.loadingMsg ? "" : this.loadingMsg
        }
    },

    watch: {
        "$root.loadingMsg": function () {
            this.msg = this.$root.loadingMsg;
        }
    },


    /*beforeCreate: function () {
        var cacheDisabled = geConfig.gannerc.sessionStorageCacheDisabled;
        if (!cacheDisabled) {
            var LocStorStats_ = new LocStorStats(geConfig.gannerc.localStorageCollectionName || 'gc');
            LocStorStats_.clearCache();
        }
    },*/

    created: function () {
        this.$root.bus.$on('loading-msg-changed', this.loadingMsgChangeCallback);
    },

    methods: {
        loadingMsgChangeCallback: function (payload) {
            this.msg = payload
        },
    }
});