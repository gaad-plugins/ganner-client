window['notificationComponentObject'] = Vue.component('notification', {
    template: '#gannerc-template-notification',
    props: ['type', 'msg'],
    data: function () {
        return {}
    },

    methods: {}
});