window['notificationsComponentObject'] = Vue.component('notifications', {
        template: '#gannerc-template-notifications',
        data: function () {
            return {
                notifications: []
            }
        },
        created: function () {
            this.$root.bus.$on('notification-push', this.notificationPushCallback);
        },
        methods: {
            notificationPushCallback: function (payload) {
                this.notifications.push(payload);
            }
        }
    }
);