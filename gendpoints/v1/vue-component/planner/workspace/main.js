window['workspaceComponentObject'] = Vue.component('workspace', {
    template: '#gannerc-template-workspace',
    data: function () {
        var theListModule = this.$store.state.theListModule;
        var aBuckets = undefined !== theListModule ? this.$store.state.theListModule.aBuckets : [];
        return {
            buckets: aBuckets,
            dataLoading: this.$root.dataLoading,
            leftPanelOpened: false,
            rightPanelOpened: false
        }
    },

    beforeMount: function () {
        if (this.buckets.length !== 0
            && undefined !== this.$store.state.theListModule
            && this.dataLoading
            // && !this.$root.dataLading
        ) {
            this.dataLoading = false;
        }
    },

    created: function () {
        this.$root.bus.$on('dataLoadingDone', this.dataLoadingDone);
        this.$root.bus.$on('theList-updated', this.dataUpdate);
    },

    methods: {
        panelToggle: function ($event, variableName) {
            if (undefined !== this[variableName] && typeof this[variableName] === "boolean") {
                this[variableName] = !this[variableName];
            }
        },
        getClass: function () {

            var classObj = {
                dataLoading: this.dataLoading,
                'vue-component-workspace': true,
                'left-panel-opened': this.leftPanelOpened,
                'right-panel-opened': this.rightPanelOpened
            };

            if (typeof this.classString !== 'undefined') {
                classObj[this.classString] = true;
            }

            return classObj;
        },

        dataUpdate: function (payload) {
            if (undefined !== this.$store.state.theListModule) {
                this.buckets = this.$root.$store.state.theListModule.aBuckets;
            }
        },
        dataLoadingDone: function (payload) {
            if (undefined === this.buckets || (this.buckets.length === 0 && undefined !== this.$store.state.theListModule)) {
                $workspace = this;
                $app = window[window.geConfig.gannerc.appVarName];
                setTimeout(function () {
                    if (undefined !== $workspace && undefined !== $workspace.$store) {
                        $workspace.buckets = $app.$store.state.theListModule.aBuckets;
                        $workspace.dataLoading = false;
                        $app.appDataLoadingOff();
                    } else {
                        debugger
                    }
                    delete $workspace;
                    delete $app;
                }, 5);
            }
        },


        add: function () {
            this.list.push({name: "Juan"});
        },
        replace: function () {
            this.list = [{name: "Edgard"}];
        },
        clone: function (el) {
            return {
                name: el.name + " cloned"
            };
        },
        onMove: function (evt) {
            this.$root.bus.$emit('bucket-add-task-cancel');
        },
        log: function (evt) {
            window.console.log(evt);
        }
    }
});