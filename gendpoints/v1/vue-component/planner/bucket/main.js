Vue.component('bucket', {
    template: '#gannerc-template-bucket',
    props: ["name", "id", "addNew"],
    data: function () {
        var addNew = undefined !== this.addNew ? this.addNew : false;
        return {
            addNew_: addNew,
            addNewVisible: addNew,
            tasks: []
        }
    },


   /* beforeCreate: function () {
        var cacheDisabled = geConfig.gannerc.sessionStorageCacheDisabled;
        if (!cacheDisabled) {
            var LocStorStats_ = new LocStorStats(geConfig.gannerc.localStorageCollectionName || 'gc');
            LocStorStats_.clearCache();
        }
    },*/

    created: function () {
        this.tasks = this.taskList;
        this.$root.bus.$on('dataLoadingDone', this.dataLoadingDoneCallback);
        this.$root.bus.$on('theList-updated', this.dataUpdate);
    },

    computed: {
        taskList: {
            get: function () {
                var usedIds = [];
                var thisBucketTasks = [];
                if (undefined === this.$store.state.theListModule) {
                    //fallback
                    return [];
                }
                var aTasks = this.$store.state.theListModule.aTasks;
                for (var i in aTasks) {
                    var oTask = aTasks[i];
                    if (usedIds.indexOf(oTask.id) !== -1) continue;
                    if (oTask.bucket_id === this.id) {
                        usedIds.push(oTask.id);
                        thisBucketTasks.push(oTask);
                    }
                }
                return thisBucketTasks;
            }
        }
    },

    methods: {
        dataUpdate: function (payload) {
            if (undefined !== this.$store.state.theListModule) {
                this.tasks = this.taskList;
            }
        },
        filterThisBucketTasks: function (aTasks) {
            var usedIds = [];
            var thisBucketTasks = [];
            for (var i in aTasks) {
                var oTask = aTasks[i];
                if (usedIds.indexOf(oTask.id) !== -1) continue;
                if (oTask.bucket_id === this.id) {
                    usedIds.push(oTask.id);
                    thisBucketTasks.push(oTask);
                }
            }
            return thisBucketTasks;
        },
        dataLoadingDoneCallback: function (payload) {
            var moduleName = payload + "Module";

            if ("theList" === payload && 0 === this.tasks.length) {
                this.tasks = this.filterThisBucketTasks(this.$store.state[moduleName].aTasks);
            }

            if ("addNewTask" === payload) {
                var moduleName = payload + "Module";
                var newTask = this.$store.state[moduleName];
                if (undefined === newTask) return

                if (this.id === newTask.bucket_id) {

                    if (undefined === this.$store.state.theListModule.aTasks) {
                        debugger
                    } else {
                        this.$store.state.theListModule.aTasks.push(this.$store.state[moduleName]);
                    }

                    this.tasks = this.taskList;
                    $this = this;
                    setTimeout(function () {
                        $this.$root.removeDataLoadedIndex(payload);
                    }, 5);
                }
            }

        },

        add: function () {
            this.list.push({name: "Juan"});
        },
        replace: function () {
            this.list = [{name: "Edgard"}];
        },
        clone: function (el) {
            return {
                name: el.name + " cloned"
            };
        },

        onMove: function (evt) {
            this.$root.bus.$emit('bucket-add-task-cancel');
        },

        log: function (evt) {
            window.console.log(evt);
        }
    }
});