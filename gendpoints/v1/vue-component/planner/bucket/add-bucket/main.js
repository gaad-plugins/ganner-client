window['add-bucketComponentObject'] = Vue.component('add-bucket', {
    template: '#gannerc-template-add-bucket',
    data: function () {
        return {
            mode_: undefined !== this.mode ? this.mode : 0,
            name: '',
            loading: false,
            storeModuleName: 'addNewBucket'
        }
    },

    validations: {
        name: {
            maxLength: window.validators.maxLength(120),
            minLength: window.validators.minLength(5),
            required: window.validators.required
        }
    },


    beforeCreate: function () {
        var cacheDisabled = geConfig.gannerc.sessionStorageCacheDisabled;
        if (!cacheDisabled) {
            var LocStorStats_ = new LocStorStats(geConfig.gannerc.localStorageCollectionName || 'gc');
            LocStorStats_.clearCache();
        }
    },

    created: function () {
        this.$root.bus.$on('bucket-add-cancel', this.bucketCancelCallback);
        this.$root.bus.$on('dataLoadingDone', this.dataLoadingDoneCallback);
    },

    methods: {

        dataLoadingDoneCallback: function (payload) {
            var moduleName = payload + "Module";
            if ("addNewBucket" === payload) {
                var newBucket = this.$store.state[moduleName];
                if (undefined === newBucket) return
                $this = this;
                setTimeout(function () {
                    $this.$root.removeDataLoadedIndex(payload);
                }, 5);
            }
        },


        requestCallback: function (data) {
            //local storage cache rebuild after origin change
            var collectionName = geConfig.gannerc.localStorageCollectionName || 'gc';
            var cacheDisabled = geConfig.gannerc.sessionStorageCacheDisabled;
            if (!cacheDisabled) {
                var cacheMng = new window.LocStorMng(collectionName);
                if (cacheMng.active) {
                    cacheMng.refresh('theList');
                }
            }
            //reset form
            this.bucketCancelCallback();
            this.$root.bus.$emit('bucket-add-success');
        },

        addNew: function () {
            this.$root.pausePreload = true;

            var headers = {
                Name: encodeURI(this.name)
            };

            this.$root.enqueueApiData(new apiDataRequest(this.storeModuleName, {
                callback: this.requestCallback,
                type: 'post',
                alive: 0,
                endpoint: this.$root.getModuleEndpoint(this.storeModuleName),
                headers: headers
            }, this.$root));
            this.loading = true;
        },

        showForm: function () {
            this.$root.bus.$emit('bucket-add-cancel');
            this.mode_ = 1;
        },

        cancelForm: function () {
            this.bucketCancelCallback();
            this.$root.bus.$emit('bucket-add-cancel');
        },

        bucketCancelCallback: function () {
            this.$root.pausePreload = false;
            this.loading = false;
            this.mode_ = 0;
            this.name = '';
        }
    }
});