Vue.component('task', {
    template: '#gannerc-template-task',
    props: ["name", "id"],
    data: function () {
        return {}
    },
    methods: {
        add: function () {
            this.list.push({name: "Juan"});
        },
        replace: function () {
            this.list = [{name: "Edgard"}];
        },
        clone: function (el) {
            return {
                name: el.name + " cloned"
            };
        },
        log: function (evt) {
            window.console.log(evt);
        }
    }
});