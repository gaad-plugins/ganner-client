window['bucket-add-taskComponentObject'] = Vue.component('bucket-add-task', {
    props: ["bucketId", "mode"],
    template: '#gannerc-template-bucket-add-task',
    data: function () {
        return {
            mode_: undefined !== this.mode ? this.mode : 0,
            name: '',
            loading: false,
            storeModuleName: 'addNewTask'
        }
    },

    validations: {
        name: {
            maxLength: window.validators.maxLength(120),
            minLength: window.validators.minLength(5),
            required: window.validators.required
        }
    },

    beforeCreate: function () {
        var cacheDisabled = geConfig.gannerc.sessionStorageCacheDisabled;
        if (!cacheDisabled) {
            var LocStorStats_ = new LocStorStats(geConfig.gannerc.localStorageCollectionName || 'gc');
            LocStorStats_.clearCache();
        }
    },

    created: function () {
        this.$root.bus.$on('bucket-add-task-cancel', this.bucketTaskCancelCallback);
    },

    methods: {

        requestCallbackFail: function (data) {

        },

        requestCallback: function (data) {
            //local storage cache rebuild after origin change
            var collectionName = geConfig.gannerc.localStorageCollectionName || 'gc';
            var cacheDisabled = geConfig.gannerc.sessionStorageCacheDisabled;
            if (!cacheDisabled) {
                var cacheMng = new window.LocStorMng(collectionName);
                if (cacheMng.active) {
                    cacheMng.refresh('theList');
                }
            }
            //reset form
            this.bucketTaskCancelCallback();
            this.$root.bus.$emit('bucket-add-task-success');
        },

        addNew: function () {
            this.$root.pausePreload = true;

            var headers = {
                Name: encodeURI(this.name)
            };
            headers['Bucket'] = null !== this.bucketId ? this.bucketId : "backlog";

            this.$root.enqueueApiData(new apiDataRequest(this.storeModuleName, {
                callback: this.requestCallback,
                callbackFail: this.requestCallbackFail,
                type: 'post',
                alive: 0,
                endpoint: this.$root.getModuleEndpoint(this.storeModuleName),
                headers: headers
            }, this.$root));
            this.loading = true;
        },

        showForm: function () {
            this.$root.bus.$emit('bucket-add-task-cancel');
            this.mode_ = 1;
        },

        cancelForm: function () {
            this.bucketTaskCancelCallback();
            this.$root.bus.$emit('bucket-add-task-cancel');
        },

        bucketTaskCancelCallback: function () {
            this.$root.pausePreload = false;
            this.loading = false;
            this.mode_ = 0;
            this.name = '';
        }
    }
});