Vue.component('gcr-link', {
    props: ['path', 'restricted', 'msg', 'needCaps', 'ifFailed', 'fallbackComponent'],

    template: '#gannerc-template-gcr-link',

    data: function () {
        return {}
    },

    methods: {
        getRoute: function () {
            var route = {
                routeSchema: this.$root.getRoute(this.path).routeSchema,
                path: this.path
            }

            if (route.routeSchema === "/not-found-404") {
                $app = this.$root;
                setTimeout(function () {
                    $app.loading = false;
                    $app.dataLoading = false;
                    $app.manifestLoading = false;
                }, 10)
            }
            return route;
        },
        clickEvent: function (e) {
            this.$root.bus.$emit('routeChangeStart', this.getRoute());
        }
    }
});