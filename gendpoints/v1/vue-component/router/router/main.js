var gcRouter = Vue.component('router', {
    props: ['routes', 'srcparam'],
    template: '#gannerc-template-router',
    data: function () {
        return {
            component_: undefined === this.getComponent() ? "index-view" : this.getComponent(),
            notFoundComponent: typeof window[geConfig.gannerc.rewrite.components['not-found']] === "function" ? window[geConfig.gannerc.rewrite.components['not-found']] : null,
            restrictedComponent: typeof window[geConfig.gannerc.rewrite.components['restricted']] === "function" ? window[geConfig.gannerc.rewrite.components['restricted']] : null,
            routesList: []
        }
    },

    created: function () {
        this.$root.bus.$on('routeChangeStart', this.routeChangeEvent);
        this.popStateEvent();
        window.addEventListener('popstate', this.popStateEvent, false);
    },

    computed: {
        currentRoute: function () {
            //@TODO Implement 404 detection here
            return this.$route.query[this.srcparam];
        },

        component: {
            set: function (value) {
                this.component_ = value;
            },
            get: function () {
                var oRegExp = new RegExp('[&]' + this.srcparam + '=([^&.]*)', 'i');
                var currentRoute = oRegExp.exec(window.location.href)[1];
                var currentRouteComponentVar = this.$root.getRoute(decodeURIComponent(currentRoute)).component;
                return typeof window[currentRouteComponentVar + 'ComponentObject'] === "function" ? currentRouteComponentVar : 'app404RouteComponentVar';
            }
        },

        params: function () {
            return this.$root.getRoute(this.currentRoute).params;
        }
    },

    beforeCreate: function () {
        this.localStorageCacheDisabled = geConfig.gannerc.localStorageCacheDisabled;
        if (!this.localStorageCacheDisabled) {
            this.LocStorStats = new LocStorStats(geConfig.gannerc.localStorageCollectionName || 'gc', 'localStorage');
            this.LocStorStats.clearCache();
        }
        this.sessionStorageCacheDisabled = geConfig.gannerc.sessionStorageCacheDisabled;
        if (!this.sessionStorageCacheDisabled) {
            this.LocStorStats = new LocStorStats(geConfig.gannerc.sessionStorageCollectionName || 'gc', 'sessionStorage');
            this.LocStorStats.clearCache();
        }
    },

    methods: {
        getComponent: function (e) {
            return this.component;
        },

        popStateEvent: function (e) {
            var currentUrl = decodeURIComponent(window.location.search);
            var params = new URLSearchParams(currentUrl);
            var srcparam = params.getAll(decodeURIComponent(this.srcparam));
            var route = this.$root.getRoute(srcparam[0]);
            var allowView = this.$root.checkUserCaps(route.name + '-view');
            var notFoundComponent = typeof window[geConfig.gannerc.rewrite.components['not-found']] === "function" ? window[geConfig.gannerc.rewrite.components['not-found']] : null;
            var restrictedComponent = typeof window[geConfig.gannerc.rewrite.components['restricted']] === "function" ? window[geConfig.gannerc.rewrite.components['restricted']] : null;

            if (allowView) {
                this.component = route ? route.component : notFoundComponent;
            } else {
                this.component = restrictedComponent ? restrictedComponent : notFoundComponent;
            }
        },

        getRouteBy__path(path) {
            var oRoute = this.$root.getRoute(path);
            if (oRoute) {
                return oRoute
            }
            return null;
        },

        getRouteBy: function (by, value) {
            if (typeof this['getRouteBy__' + by] === "function") return this['getRouteBy__' + by](value);
            return null;
        },

        buildUrl: function (path) {
            var oRegExp = new RegExp('[&]' + this.srcparam + '=[^&.]*', 'i');
            var sUrlBase = window.location.href.replace(oRegExp, "");
            return sUrlBase + (sUrlBase.indexOf('?') > -1 ? "&" : "?") + decodeURIComponent(this.srcparam) + '=' + path;
        },

        applyManifestData: function (data) {
            var VueTemplateHandlePrefix = data.VueTemplateHandlePrefix;
            for (var handle in data.componentsData) {

                this.$store.commit('VueViewLoadedAdd', data.routeSchema);
                this.$store.commit('VueComponentsLoadedAdd', handle);

                var sID = VueTemplateHandlePrefix + handle;
                var item = data.componentsData[handle];

                //@TODO implement solution without jQuery (vanilla)
                var linkTag = jQuery("<link>")
                    .attr({
                        href: item.style,
                        type: 'text/css',
                        rel: 'stylesheet',
                        id: sID + 'stylesheet'
                    });
                $('head').append(linkTag);

                var scriptTag = jQuery("<script>")
                    .attr({
                        type: 'template/javascript',
                        id: sID
                    });
                scriptTag.html(item.template);
                $('body').append(scriptTag);

                var scriptTag = jQuery("<script>")
                    .attr({
                        type: 'text/javascript'
                    });
                scriptTag.html(item.appCode);
                $('body').append(scriptTag);

            }
            return true;
        },

        routeChangeManifestLoaded: function (data) {
            if (this.applyManifestData(data.data.data)) {
                var requestData = data.data.data;
                var originName = requestData['Ge-Data-Origin'];
                var alive = requestData['Cache-Alive'];

                if (!this.localStorageCacheDisabled) {
                    //local storage cache handling
                    var cacheMng = new window.LocStorMng(geConfig.gannerc.localStorageCollectionName, 'localStorage');
                    if (cacheMng.active) {
                        cacheMng.add(originName, new window.LocStorItem(data, alive), $manifestRequest);
                        this.LocStorStats.clearCache(true);
                    }
                }

                this.component = data.data.data.component;
                this.$root.bus.$emit('routeChangeSuccess');
                this.$root.manifestLoading = false;
            }
        },

        routeChangeEvent: function (payload) {
            var route = this.getRouteBy('path', payload.path);
            var allowView = this.$root.checkUserCaps(route.name + '-view');
            var prevRoute = this.$root.pushedStates.length > 0 ? this.$root.pushedStates[this.$root.pushedStates.length - 1] : false;
            if (prevRoute && prevRoute === payload.path) {
                if (!allowView) {
                    this.component = this.restrictedComponent ? this.restrictedComponent : this.notFoundComponent;
                } else {
                    this.component = route.component;
                }
                this.$root.bus.$emit('routeChangeSuccess');
                return;
            }

            this.$root.pushedStates.push(payload.path);
            history.pushState({
                id: ""
            }, "title", this.buildUrl(payload.path));

            if (!allowView) {
                this.component = this.restrictedComponent ? this.restrictedComponent : this.notFoundComponent;
                this.$root.bus.$emit('routeChangeSuccess');
                return;
            }

            if (!this.localStorageCacheDisabled) {
                this.LocStorStats.clearCache()
                var cacheMng = new window.LocStorMng(geConfig.gannerc.localStorageCollectionName, 'localStorage');
            }

            if (this.$store.getters.VueViewLoadedG.indexOf(payload.routeSchema) === -1) {
                if (!this.localStorageCacheDisabled && cacheMng.active) {
                    $storageItem = cacheMng.get(payload.routeSchema);
                    if ($storageItem) {
                        var requestData = $storageItem.output.requestData;
                        $manifestRequest = new apiDataRequest(requestData.headers['Ge-Data-Origin'], requestData.data, this.$root, true);
                        this.$root.appLoadingOn();
                        this.$root.manifestLoading = true;
                        $this = this;
                        setTimeout(function () {
                            $this.routeChangeManifestLoaded($storageItem.output.response);
                        }, 5);
                        return;
                    }
                }

                if (this.$root.dataLoadedIndex.indexOf(payload.routeSchema) !== -1) return;

                $manifestRequest = new apiDataRequest(payload.routeSchema, {
                    loadingMsg: "Loading manifest",
                    type: 'post',
                    alive: 10000000000,
                    endpoint: "v1/data/vue-components/load-manifest",
                    headers: {
                        'RouteSchema': payload.routeSchema,
                        'Route': payload.path,
                        'Ignore': Store.getters.VueComponentsLoadedG.join(","),
                        'Component': route.component,
                    },
                    callback: this.routeChangeManifestLoaded
                });

                this.$root.loadingMsg = "Loading manifest";
                this.$root.manifestLoading = true;
                $manifestRequest.makeDataRequest();
                this.$root.dataLoadedIndex.push(payload.routeSchema);

            } else {
                this.component = route.component;
                this.$root.bus.$emit('routeChangeSuccess');
            }
        }
    }
});