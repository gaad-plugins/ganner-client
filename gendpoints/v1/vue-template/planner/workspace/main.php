<?php
//this is workspace HTML template
?>
<div :class="getClass()" v-if="!dataLoading">

    <div class="workspace__panel workspace__panel--left">
        <div class="workspace__panel__content">

            <bucket class="workspace__list-group-item workspace__backlog-bucket"
                    :name="$root.tr('Backlog')"
                    :id="0"
                    :addNew="true"
            >
            </bucket>


        </div>
        <div class="workspace__panel__tool">
            <div v-if="leftPanelOpened" @click="panelToggle($event, 'leftPanelOpened')">{{ $root.tr('Close') }}</div>
            <div v-else @click="panelToggle($event, 'leftPanelOpened')">{{ $root.tr('Open') }}</div>
        </div>
    </div>


    <div class="workspace__crop-window">
        <draggable class="workspace__list-group" :list="buckets" group="buckets"
                   :move="onMove"
                   @change="log">
            <bucket
                    class="list-group-item col"
                    v-for="(element, index) in buckets"
                    :key="element.id"
                    :name="element.name"
                    :id="element.id"
                    :addNew="true"
            ></bucket>
        </draggable>

        <restricted-access
                :ifFailed="'hide'"
                :needCaps="'create-bucket'">
            <template v-slot:restricted-data>
                <add-bucket class="workspace__ist-group-item workspace__add-bucket vue-component-bucket"></add-bucket>
            </template>
        </restricted-access>

    </div>

    <div class="workspace__panel workspace__panel--right">
        <div class="workspace__panel__content">
            <bucket class="workspace__ist-group-item workspace__done-bucket"
                    :name="$root.tr('Done')"
                    :id="-1"
            >
            </bucket>
        </div>
        <div class="workspace__panel__tool">
            <div v-if="rightPanelOpened" @click="panelToggle($event, 'rightPanelOpened')">{{ $root.tr('Close') }}</div>
            <div v-else @click="panelToggle($event, 'rightPanelOpened')">{{ $root.tr('Open') }}</div>
        </div>
    </div>


</div>
<loading v-else :loading-msg="true"></loading>