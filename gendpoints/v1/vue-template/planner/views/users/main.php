<div class="vue-component-users">
    <h1>View users</h1>

    <restricted-access v-if="!dataLoading"
                       :ifFailed="'msg'"
                       :needCaps="'ganner-users-mng'">
        <template v-slot:restricted-data>
            <users-list></users-list>
        </template>
    </restricted-access>
    <loading v-else :loading-msg="true"></loading>
</div>
