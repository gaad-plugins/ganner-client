<?php
//this is task HTML template
?>
<div class="vue-component-task">

    <h1>widok task</h1>
    taskid: {{taskID }}
    <br>

    <div v-if="!dataLoading">tutaj task view content</div>
    <loading v-else :loading-msg="true"></loading>

    <gcr-link :path="'/'">back</gcr-link>
</div>
