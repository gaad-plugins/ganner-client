<div class="vue-component-restricted-access" v-if="!(ifFailed == 'hide' && !authorized)">
    <slot v-if="authorized"
          name="restricted-data"></slot>
    <div v-else>
        <div v-if="ifFailed == 'msg'">{{ msg ? msg : $root.tr('Access denied to this part for current user.') }}</div>
        <div v-if="ifFailed == 'component'">
            <component v-if="component_ !== ''" :is="component_"></component>
        </div>
    </div>
</div>
