<?php
//this is notifications HTML template
?>
<div class="vue-component-notifications">
    <notification v-for="(notification, index) in notifications"
                  :type="notification.type"
                  :msg="notification.msg"
    ></notification>
</div>
