<?php
//this is users-list HTML template
?>
<div class="vue-component-users-list">
    <user-record v-for="(user, index) in users" :key="user.id" :data="user"></user-record>
</div>
