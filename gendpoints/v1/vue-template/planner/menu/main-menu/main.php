<?php
//this is main-menu HTML template
?>
<div class="vue-component-main-menu">
    <ul>

        <li>
            <gcr-link :restricted="true"
                      :ifFailed="'hide'"
                      :needCaps="'users-view'"
                      :path="'/users'">{{ $root.tr('Users') }}
            </gcr-link>
        </li>

        <li>
            <gcr-link :restricted="true"
                      :ifFailed="'hide'"
                      :needCaps="'index-view'"
                      :path="'/'">{{ $root.tr('Tasks') }}
            </gcr-link>
        </li>
        <li>
            <gcr-link :restricted="true"
                      :ifFailed="'hide'"
                      :needCaps="'nowhere-view'"
                      :path="'/nowhere'">{{ $root.tr('404 test') }}
            </gcr-link>
        </li>
        <li>
            <gcr-link :restricted="true"
                      :ifFailed="'hide'"
                      :needCaps="'create-view'"
                      :path="'/create-task'">{{ $root.tr('Create task') }}
            </gcr-link>
        </li>
        <li>
            <gcr-link :restricted="true"
                      :ifFailed="'hide'"
                      :needCaps="'activity-view'"
                      :path="'/activity'">{{ $root.tr('Activity') }}
            </gcr-link>
        </li>
        <li>
            <gcr-link :restricted="true"
                      :ifFailed="'hide'"
                      :needCaps="'notes-view'"
                      :path="'/notes'">{{ $root.tr('Notes') }}
            </gcr-link>
        </li>
        <li>
            <gcr-link :restricted="true"
                      :ifFailed="'hide'"
                      :needCaps="'labels-view'"
                      :path="'/labels'">{{ $root.tr('Labels') }}
            </gcr-link>
        </li>
        <li>
            <gcr-link :restricted="true"
                      :ifFailed="'hide'"
                      :needCaps="'trash-view'"
                      :path="'/trash'">{{ $root.tr('Trash') }}
            </gcr-link>
        </li>
        <li>
            <gcr-link :restricted="true"
                      :ifFailed="'hide'"
                      :needCaps="'done-view'"
                      :path="'/done'">{{ $root.tr('Done tasks') }}
            </gcr-link>
        </li>
    </ul>
</div>
