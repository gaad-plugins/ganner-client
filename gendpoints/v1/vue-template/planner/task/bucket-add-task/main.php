<?php
//this is bucket-add-task HTML template
?>
<div class="vue-component-bucket-add-task">


    <div v-if="loading">
        <loading></loading>
    </div>

    <ul v-if="mode_===0">
        <li>
            <input type="button"
                   v-on:click="showForm"
                   :value="$root.tr('Add task')"
            >
        </li>
    </ul>

    <ul v-if="mode_===1" class="add-form">
        <li>
            title:
            <input
                    v-model="name"
                    type="text"
            >
        </li>
        <li>
            <input type="button"
                   v-on:click="addNew"
                   :value="$root.tr('Add task')"
                   :disabled="$v.$invalid"
            >
            <input type="button"
                   v-on:click="cancelForm"
                   :value="$root.tr('Cancel')"
            >
        </li>
    </ul>

    <loading v-if="mode_===-1"></loading>


</div>
