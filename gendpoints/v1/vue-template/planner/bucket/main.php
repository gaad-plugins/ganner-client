<?php
//this is bucket HTML template
?>
<div class="vue-component-bucket bucket">
    Bucket: {{ name }}<br>
    count: {{ tasks.length }}


    <draggable class="bucket__dragable_group" v-model="tasks" group="tasks"
               :move="onMove"
               @change="log">
        <task
                class="bucket-group-item"
                v-for="(element, index) in tasks"
                :key="element.id"
                :name="decodeURI(element.name)"
                :id="element.id"
        ></task>
    </draggable>

    <div class="bucket__footer">
        {{ id }}
        <slot name="footer">
            <restricted-access
                    :ifFailed="'hide'"
                    :needCaps="'create-task'">
                <template v-slot:restricted-data>
                    <bucket-add-task
                            :bucket-id="id"
                            v-if="addNewVisible"
                    ></bucket-add-task>
                </template>
            </restricted-access>
        </slot>
    </div>

</div>
